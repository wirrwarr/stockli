from django.contrib import admin
from stockli.models import Index, Stock, Dividend, \
                           KeyFigures, Appointment, \
                           Market, StockPrice, IndexPrice, \
                           KeyFigures2, Portfolio, \
                           PortfolioContent, StockQuantity

class IndexAdmin(admin.ModelAdmin):
    pass

class StockAdmin(admin.ModelAdmin):
    list_display = ("isin", "name")
    list_display_links = list_display

class DividendAdmin(admin.ModelAdmin):
    list_display = ("year", "stock", "money")
    list_display_links = list_display

class KeyFiguresAdmin(admin.ModelAdmin):
    list_display = ("year", "stock", "per", "pcr", "psr", \
                    "pbr", "roi", "cashflow", "liquidity", \
                    "equity_ratio", "debt_ratio", "return_on_sales")
    list_display_links = list_display

class KeyFiguresAdmin2(admin.ModelAdmin):
    list_display = ("year", "stock", "per", "roe", \
                    "equity_ratio", "ebit_marge")
    list_display_links = list_display

class AppointmentAdmin(admin.ModelAdmin):
    list_display = ("stock", "date", "typ")
    list_display_links = list_display

class MarketAdmin(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = list_display

class StockPriceAdmin(admin.ModelAdmin):
    list_display = ("stock", "date", "start", "end", "low", "high")
    list_display_links = list_display

class IndexPriceAdmin(admin.ModelAdmin):
    list_display = ("index", "date", "start", "end", "low", "high")
    list_display_links = list_display

class PortfolioAdmin(admin.ModelAdmin):
    list_display = ("name", )
    list_display_links = list_display

class PortfolioContentAdmin(admin.ModelAdmin):
    list_display = ("date", "portfolio", "stock")
    list_display_links = list_display

class StockQuantityAdmin(admin.ModelAdmin):
    list_display = ("stock", "date", "quantity")
    list_display_links = list_display

admin.site.register(Index, IndexAdmin)
admin.site.register(Stock, StockAdmin)
admin.site.register(Dividend, DividendAdmin)
admin.site.register(KeyFigures, KeyFiguresAdmin)
admin.site.register(KeyFigures2, KeyFiguresAdmin2)
admin.site.register(Appointment, AppointmentAdmin)
admin.site.register(Market, MarketAdmin)
admin.site.register(StockPrice, StockPriceAdmin)
admin.site.register(IndexPrice, IndexPriceAdmin)
admin.site.register(Portfolio, PortfolioAdmin)
admin.site.register(PortfolioContent, PortfolioContentAdmin)
admin.site.register(StockQuantity, StockQuantityAdmin)
