import re
import calendar
import asyncio
import aiohttp
from datetime import date, timedelta
from django.db import connection
from .models import Stock

class DateMod():
    def __init__(self, day=None, month=None, year=None, date_obj=None):
        if day is None:
            self.d = date_obj
        else:
            self.d = date(day=day, month=month, year=year)

    def mod_month(self, months):
        month = self.d.month - 1 + months
        year = int(self.d.year + month / 12)
        month = month % 12 + 1
        day = min(self.d.day,calendar.monthrange(year,month)[1])
        return date(year,month,day)

    def mod_year(self, years):
        return self.mod_month(12*years)

def find_last_weekday_month(month, year):
    """
    Finde den letzten Wochentag eines Monats.
    """
    day,e = calendar.monthrange(year, month)
    dtmp = date(day=e, month=month, year=year)
    if dtmp.isoweekday() > 5:
        return find_pre_post_days(dtmp)[0]
    else:
        return dtmp

def find_last_day_month(month, year):
    """
    Gibt Date Objekt letzter Tag eines Monats zurück
    """
    day,e = calendar.monthrange(year, month)
    return date(day=e, month=month, year=year)


def find_pre_post_days(d):
    """
    Findet einen Tag nach und vor dem übergebenen Datum.
    Die Tage dürfen keinen Wochenendtag sein.
    """
    pre_day = d - timedelta(days=1)
    post_day = d + timedelta(days=1)

    if pre_day.isoweekday() > 5:
        pre_day = pre_day - timedelta(days=pre_day.isoweekday()-5)
    if post_day.isoweekday() > 5:
        post_day = post_day + timedelta(days=(post_day.isoweekday()-8)*-1)

    return (pre_day, post_day)


def make_date_object(d):
    """
    Baue Date Objekt aus DD.MM.YY oder DD.MM.YYY oder D.M.YYY
    """
    dtmp = d.split(".")
    for i,n in enumerate(dtmp):
        dtmp[i] = int(n)
        if n[0] == 0:
            dtmp[i] = n[1:]
    if dtmp[-1] < 2000:
        dtmp[-1] = dtmp[-1] + 2000

    return date(day=dtmp[0], month=dtmp[1], year=dtmp[2])

def clean_parsed_numbr(s):
    return re.sub('[!"§$%&/()=?€]*', "", s).strip()

def make_float(s):
    """
    Baut aus einem String 8.15,44
    eine Python float Objekt
    """
    tmp = s.split(",")
    tmp[0] = tmp[0].replace(".", "")
    if len(tmp) == 2:
        new_numb = ".".join(tmp)
    else:
        new_numb = tmp[0]

    if new_numb[0] == "-":
        return float(new_numb[1:])*-1
    else:
        return float(new_numb)


def make_stock_url_ready(name):
    return name.replace(" ", "_")

def make_stock_onvistaurl_ready(name):
    return name.replace(" ", "-")

def make_index_url_ready(index):
    return index.replace(" ", "_")

def check_luhn(purportedCC=''):
    sum = 0
    parity = len(purportedCC) % 2
    for i, digit in enumerate([int(x) for x in purportedCC]):
        if i % 2 == parity:
            digit *= 2
        if digit > 9:
            digit -= 9
        sum += digit
    return sum % 10 == 0

def replace_alpha(string):
    """
    Bereite String für luhn check vor.
    Ersetze alle Buchstaben durch Zahlen (nach Alphabet) + 9.
    """
    s = []
    for c in string:
        if c.isalpha():
            s.append(str(ord(c.upper())-64+9))
        else:
            s.append(c)

    return "".join(s)

def is_isin(isin):
    """
    Überprüft der Übergeben String eine ISIN sein könnte. 
    Die Länge muss 12 sein.
    Stelle 1 und 2 müssen Buchstaben sein
    Stelle 12 muss eine Zahl sein und der korrekten Prüfsumme entsprechen.
    """
    isin = isin.upper()

    if len(isin) != 12:
        return False

    if re.search(r"^[A-Z]{2}", isin) is None:
        return False

    if isin[-1].isalpha():
        return False

    if not check_luhn(replace_alpha(isin.lower())):
        return False

    return True

def insert_values(values, fields, table):
    fields_str = "("+ ",".join(fields) +")"
    base_sql = "INSERT INTO "+ table + fields_str +" VALUES "
    values_sql = []
    values_data = []

    for value_list in values:
        placeholders = ['%s' for i in range(len(value_list))]
        values_sql.append("(%s)" % ','.join(placeholders))
        values_data.extend(value_list)

    sql = '%s%s' % (base_sql, ', '.join(values_sql))

    curs = connection.cursor()
    curs.execute(sql, values_data)

def query_indices_stocks(indices):
    base_sql = "select a.isin, a.name from stockli_stock as a, stockli_stock_indizes as b"
    where = ["b.index_id = %s" % indices[0].pk]
    for i in indices[1:]:
        where.append("or b.index_id = %s" % i.pk)

    sql = "%s where a.isin = b.stock_id and (%s) group by a.isin" % (base_sql, " ".join(where))
    curs = connection.cursor()
    curs.execute(sql)

    result = curs.fetchall()
    result = sorted(result, key=lambda x: x[1])
    if len(result) == 0:
        return ()

    return tuple([Stock(isin=isin, name=name)
                     for isin,name in result])

def query_stock_indices(stocks=None):
    stocks_with_index_sql = \
            """select stock.isin, stock.name, index.id, index.name 
               from stockli_stock as stock, 
                    stockli_index as index, 
                    stockli_stock_indizes as ref
               where stock.isin = ref.stock_id
               and  index.id = ref.index_id"""

    stocks_without_index_sql = \
            """select stock.isin, stock.name
               from stockli_stock as stock
               where not exists (
                            select 1 
                            from stockli_stock_indizes as ref 
                            where stock.isin = ref.stock_id)"""

    sp_stocks = ""
    if stocks is not None:
        sp_stocks = " and (stock.isin = '"+ stocks[0].isin +"'"
        for s in stocks[1:]:
            sp_stocks += "or stock.isin = '"+ s.isin +"'"
        sp_stocks += ")"

    curs = connection.cursor()
    curs.execute(stocks_with_index_sql + sp_stocks)
    stocks_with_index_result = curs.fetchall()
    curs.execute(stocks_without_index_sql + sp_stocks)
    stocks_without_index_result = curs.fetchall()

    result = {}
    for i in stocks_with_index_result:
        result[i[0]] = {"name": i[1], "index_id": i[2], "index": i[3]}

    for i in stocks_without_index_result:
        result[i[0]] = {"name": i[1], "index": None, "index_id": None}

    if len(result) == 0:
        return {}

    return result

def is_elected(stock, points, stock_indices):
    min_points = {
                  "SDAX": 7, 
                  "MDAX": 7,
                  "DAX": 5,
                  "Euro Stoxx 50": 5,
                  "Down Jones": 5,
                  "Nasdaq 100": 5,
                  "None": 6,
                  }

    try:
        v = stock_indices[stock.isin]
        if v["name"] is None:
            p = min_points["None"]
        else:
            p = min_points[v["index"]]
    except KeyError:
        p = min_points["None"]


    if p <= points:
        return True
    else:
        return False

def loop_process_pages(tasks):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = [asyncio.async(process_page(t)) for t in tasks]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()

@asyncio.coroutine
def process_page(task):
    html = yield from get(task.url, task.logger)
    task.handler(html)

@asyncio.coroutine
def get(url, logger):
    header = {"User-agent": 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36',
              "Content-Type": 'text/html'}
    response = yield from aiohttp.request('GET', url, headers=header)
    if response.status == 200:
        logger.info("%s OK!" % (url,))
    else:
        logger.error("%s FAILED!" % (url,))
    return (yield from response.read_and_close())

class RequestTaskData():
    """
    Stellt alle Daten bereit die benötigt werden
    um einen AsyncTask auszuführen der eine bestimmt
    URL abfrägt und diese verarbeite. Alle Nachrichten
    seine es Fehler, Hinweise, etc. werden an den 
    Logger übergeben. Der Logger findet anwendung sowohl
    innerhalb der handler Funktion sowie beim abrufen
    der URL. Der Logger wurde allerdings dem handler 
    schon zuvor übergeben und wird nicht mehr beim aufruf
    des Handler mit übergeben.
    """
    def __init__(self, url, handler, logger):
        # Handler wird folgendermaßen aufgerufen handler(data)
        self.handler = handler
        self.url = url
        self.logger = logger

class MemoryLogger():
    """
    Speichert Lognachrichten in Memory.
    Zweck, Lognachrichten die wärend dem ablauf einer
    Asyncen EventLoop auftretten zwischen zu speichern.
    Sodas sie nach beenden der EventLoop noch abgerufen werden können.
    """
    def __init__(self, name):
        self.name = name
        self.__msgs = {"error": [],
                     "warn": [],
                     "info": [],
                     "debug": []}
    def __msg(self, level, msg):
        self.__msgs[level].append(msg)

    def error(self, msg):
        self.__msg("error", msg)

    def warn(self, msg):
        self.__msg("warn", msg)

    def info(self, msg):
        self.__msg("info", msg)

    def debug(self, msg):
        self.__msg("debug", msg)

    def __get_logs(self, level):
        return self.__msgs[level]

    def get_error(self):
        return self.__get_logs("error")

    def get_warn(self):
        return self.__get_logs("warn")

    def get_info(self):
        return self.__get_logs("info")

    def get_debug(self):
        return self.__get_logs("debug")

    def get_all_logs(self):
        return self.__msgs

class DataQueue():
    """
    Speicher Objekte in einer Liste.
    Einträge aus der Liste können nicht gelöscht oder geändert werden.
    Es können nur Objekte hinzugefügt werden
    Zweck, Daten aus einer Asyncen EventLoop herauszubekommen.
    """
    def __init__(self, name):
        self.name = name
        self.__objects = []

    def add(self, obj):
        self.__objects.append(obj)

    def objects(self):
        return self.__objects

request_urls = {
                "appointments": "http://www.finanzen.net/termine/%s",
                "keyfigures": "http://www.wallstreet-online.de/aktien/%s/unternehmensprofil",
                "keyfigures_2": "http://www.onvista.de/aktien/fundamental/%s-Aktie-%s",
                "stockprices_month": "http://www.ariva.de/%s/historische_kurse?month=%s",
                "indexprices_month": "http://www.ariva.de/%s-index/historische_kurse?month=%s",
                "dividents": "http://www.finanzen.net/dividende/%s",
                "indices": "http://www.finanzen.net/index/%s/Werte",
                "stock_quantity": "http://www.onvista.de/aktien/unternehmensprofil/%s-Aktie-%s",
                }
