# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date', models.DateField(verbose_name='date')),
                ('typ', models.IntegerField(verbose_name='typ')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Balance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('year', models.IntegerField(verbose_name='year')),
                ('circulating_assets', models.FloatField(verbose_name='Umlaufvermögen', null=True, blank=True)),
                ('capital_assets', models.FloatField(verbose_name='Anlagevermögen', null=True, blank=True)),
                ('current_liabilities', models.FloatField(verbose_name='kurzfristiges Verbindlichkeiten', null=True, blank=True)),
                ('fixed_liabilities', models.FloatField(verbose_name='langfristige Verbindlichkeiten', null=True, blank=True)),
                ('equity', models.FloatField(verbose_name='Eigenkapital', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Dividend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('year', models.IntegerField(verbose_name='year')),
                ('money', models.FloatField(verbose_name='money')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Index',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('name', models.CharField(verbose_name='name', max_length=500)),
                ('country', models.CharField(verbose_name='country', choices=[('de', 'Deutschland')], blank=True, max_length=500, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='IndexPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date', models.DateField(verbose_name='date')),
                ('start', models.FloatField(verbose_name='Startwert', null=True, blank=True)),
                ('end', models.FloatField(verbose_name='Schlusswert', null=True, blank=True)),
                ('high', models.FloatField(verbose_name='Höchstwert', null=True, blank=True)),
                ('low', models.FloatField(verbose_name='Niedrigsterwert', null=True, blank=True)),
                ('index', models.ForeignKey(to='stockli.Index')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KeyFigures',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('year', models.IntegerField(verbose_name='year')),
                ('per', models.FloatField(verbose_name='kgv', null=True, blank=True)),
                ('pcr', models.FloatField(verbose_name='kcv', null=True, blank=True)),
                ('psr', models.FloatField(verbose_name='kuv', null=True, blank=True)),
                ('pbr', models.FloatField(verbose_name='kbv', null=True, blank=True)),
                ('roi', models.FloatField(verbose_name='roi', null=True, blank=True)),
                ('cashflow', models.FloatField(verbose_name='cashflow', null=True, blank=True)),
                ('liquidity', models.FloatField(verbose_name='Liquidität G1', null=True, blank=True)),
                ('equity_ratio', models.FloatField(verbose_name='Eigenkaptialquote', null=True, blank=True)),
                ('debt_ratio', models.FloatField(verbose_name='Fremdkapitalquote', null=True, blank=True)),
                ('return_on_sales', models.FloatField(verbose_name='Umsatzrentabilität/EBIT-Marge', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='KeyFigures2',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('year', models.IntegerField(verbose_name='year')),
                ('per', models.FloatField(verbose_name='kgv', null=True, blank=True)),
                ('roe', models.FloatField(verbose_name='roe', null=True, blank=True)),
                ('equity_ratio', models.FloatField(verbose_name='Eigenkaptialquote', null=True, blank=True)),
                ('ebit_marge', models.FloatField(verbose_name='EBIT-Marge', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Market',
            fields=[
                ('id', models.IntegerField(verbose_name='Börsenid', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='Börsenname', max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfitAndLost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('year', models.IntegerField(verbose_name='year')),
                ('ebit', models.FloatField(verbose_name='operativer Gewinn', null=True, blank=True)),
                ('revenue', models.FloatField(verbose_name='Umsatz', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stock',
            fields=[
                ('isin', models.CharField(verbose_name='isin', serialize=False, max_length=500, primary_key=True)),
                ('name', models.CharField(verbose_name='name', max_length=500)),
                ('indizes', models.ManyToManyField(to='stockli.Index', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StockPrice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date', models.DateField(verbose_name='date')),
                ('start', models.FloatField(verbose_name='Startwert', null=True, blank=True)),
                ('end', models.FloatField(verbose_name='Schlusswert', null=True, blank=True)),
                ('high', models.FloatField(verbose_name='Höchstwert', null=True, blank=True)),
                ('low', models.FloatField(verbose_name='Niedrigsterwert', null=True, blank=True)),
                ('pieces', models.IntegerField(verbose_name='Stück', null=True, blank=True)),
                ('market', models.ForeignKey(to='stockli.Market')),
                ('stock', models.ForeignKey(to='stockli.Stock')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='stockprice',
            unique_together=set([('date', 'stock', 'market')]),
        ),
        migrations.AddField(
            model_name='profitandlost',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='profitandlost',
            unique_together=set([('year', 'stock')]),
        ),
        migrations.AddField(
            model_name='keyfigures2',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='keyfigures2',
            unique_together=set([('year', 'stock')]),
        ),
        migrations.AddField(
            model_name='keyfigures',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='keyfigures',
            unique_together=set([('year', 'stock')]),
        ),
        migrations.AlterUniqueTogether(
            name='indexprice',
            unique_together=set([('date', 'index')]),
        ),
        migrations.AddField(
            model_name='dividend',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='dividend',
            unique_together=set([('year', 'stock')]),
        ),
        migrations.AddField(
            model_name='balance',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='balance',
            unique_together=set([('year', 'stock')]),
        ),
        migrations.AddField(
            model_name='appointment',
            name='stock',
            field=models.ForeignKey(to='stockli.Stock'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='appointment',
            unique_together=set([('date', 'stock', 'typ')]),
        ),
    ]
