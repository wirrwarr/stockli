from bs4 import BeautifulSoup
from ..utils import is_isin, make_float, \
                    clean_parsed_numbr
from .utils import ParseResult

all_keyfigures_keys = {"roe": None, "ebit_marge": None, \
                       "per": None, "equity_ratio": None}

def parse_stock_quantity_page(html):
    errors = []
    b = BeautifulSoup(html)

    table = b.select("table.MARKT")
    if len(table) != 1:
        errors.append("Find to many tables with class MARKT")
        return ParseResult(data=None, errors=errors)

    for row in table[0].find_all("tr"):
        col_text = row.find("th").get_text()
        if col_text == "Anzahl Aktien":
            q = clean_parsed_numbr(row.find("td").get_text())
            q = int(make_float(q.replace(" Stk.", "")))
            return ParseResult(data=[q], errors=errors)

def parse_keyfigures_page_2(html):
    errors = []
    b = BeautifulSoup(html)
    tables = b.select("article.KENNZAHLEN div table")

    if len(tables) != 8:
        errors.append("parse_keyfigures_page_2:Not enough tables found")
        return None

    data = {}
    for table in tables:
        tmp = handle_table(table)
        #print(tmp)
        for year,figures in tmp.items():
            if year not in data:
                data[year] = all_keyfigures_keys.copy()
            for k,v in figures.items():
                if v is not None:
                    data[year][k] = v
                    #print("%s %s %s" % (year, k, v))

    #print(data)
    return ParseResult(data=data, errors=errors)

def handle_table(table):
    allowed_keynames = {"Eigenkapitalrendite": "roe", \
                        "KGV": "per", \
                        "EBIT-Marge": "ebit_marge", \
                        "Eigenkapitalquote": "equity_ratio"}

    years = get_years(table.find_all("th"))
    data = {}
    for row in table.find_all("tr")[1:]:
        keyname = ""
        l = len(row.find_all("td"))
        relevant_index = l-len(years)
        for i,col in enumerate(row.find_all("td")):
            if i == 0:
                # Kennzahl Name
                keyname = col.get_text().strip()
                #print(keyname)
                if keyname not in allowed_keynames.keys():
                    #errors.append("%s is not a keyname" % keyname)
                    break
            elif i >= relevant_index:
                vtmp = col.get_text().replace("%", "").strip()

                if len(vtmp) == 0: 
                    break
                if vtmp == "-" and len(vtmp) == 1:
                    break

                try:
                    numb = make_float(vtmp)
                except ValueError:
                    errors.append("%s is not a number" % vtmp)
                ktmp = allowed_keynames[keyname]
                year = years[i-relevant_index]
                if year not in data:
                    data[year] = all_keyfigures_keys.copy()

                data[year][ktmp] = numb 

    return data

def get_years(header):
    years = []
    for i,th in enumerate(header):
        if i > 0:
            y = th.get_text().strip()
            if len(y.split("/")) == 2:
                y_tmp = y.split("/")[1]
                try:
                    y_tmp.index("e")
                    y_tmp = int(y_tmp[0:2]) + 2000
                    y = "%se" % str(y_tmp) 
                except ValueError:
                    y = str(int(y_tmp) + 2000)

            if len(y) == 0:
                continue

            try:
                years.append(y)
            except ValueError:
                errors.append("%s is not a year" % y)

    #print(years)
    return years
