from bs4 import BeautifulSoup
from ..utils import is_isin, make_float
from .utils import ParseResult

def parse_keyfigures_page(html):
    allowed_keynames = {"Kurs-Gewinn-Verhältnis (KGV)": "per",
                        "Kurs-Cash-Flow-Verhältnis (KCV)": "pcr",
                        "Kurs-Umsatz-Verhältnis (KUV)": "psr",
                        "Kurs-Buchwert-Verhältnis (KBV)": "pbr",
                        "Umsatzrentabilität": "return_on_sales",
                        "Return on Investment": "roi",
                        "Eigenkapitalquote": "equity_ratio",
                        "Fremdkapitalquote": "debt_ratio",
                        "\u200eLiquidität 1. Grades": "liquidity"}
    all_keyfigures_keys = {"per": -1, "pcr": -1, "psr": -1, \
                          "pbr": -1, "return_on_sales": -1, \
                          "roi": -1, "equity_ratio": -1, \
                          "debt_ratio": -1, "liquidity": -1}
    errors = []
    b = BeautifulSoup(html)
    t = b.find_all("table", class_="t-data")[0]
    years = []
    for i,th in enumerate(t.find_all("th")):
        if i > 0:
            y = th.get_text()
            try:
                years.append(int(y))
            except ValueError:
                errors.append("%s is not a year" % y)

    data = {}
    for row in t.find_all("tr")[1:]:
        keyname = ""
        for i,col in enumerate(row.find_all("td")):
            if i == 0:
                # Kennzahl Name
                keyname = col.get_text().strip()
                #print(keyname)
                if keyname not in allowed_keynames.keys():
                    #errors.append("%s is not a keyname" % keyname)
                    break
            else:
                vtmp = col.get_text().replace("%", "").strip()

                if len(vtmp) == 0: 
                    break
                if vtmp == "-" and len(vtmp) == 1:
                    break

                try:
                    numb = make_float(vtmp)
                except ValueError:
                    errors.append("%s is not a number" % vtmp)
                ktmp = allowed_keynames[keyname]
                year = years[i-1]
                #print("%s -> %s" % (ktmp, vtmp))
                if year not in data:
                    data[year] = all_keyfigures_keys.copy()

                data[year][ktmp] = numb 
    #print(errors)
    #print(data)
    return ParseResult(data=data, errors=errors)
