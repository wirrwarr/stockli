from bs4 import BeautifulSoup
from ..utils import make_float, clean_parsed_numbr
from .utils import ParseResult

def parse_stockprices_month_page(html):
    b = BeautifulSoup(html)
    t = b.select("div.abstand table.line")[0]
    data = []
    errors = []
    data_tmp = []
    for rows in t.find_all("tr")[1:-1]:
        for i,col in enumerate(rows.find_all("td")):
            if i == 7:
                date_tmp = None
                break
            if i == 5:
                continue 
            if i == 0:
                date_tmp = col.get_text()
                if len(date_tmp.split(".")) != 3:
                    errors.append("%s is not a date" % date_tmp)
                    break
                data_tmp.append(date_tmp)
            else:
                tmp = clean_parsed_numbr(col.get_text())
                if len(tmp) == 0:
                    errors.append("error with value %s" % tmp)
                    break
                try:
                    f = make_float(tmp)
                except ValueError:
                    f = -1
                    errors.append("%s is not a numbr" % tmp)
                data_tmp.append(f)

        data.append(tuple(data_tmp))
        data_tmp = []

    return ParseResult(data=tuple(data), errors=errors)

def parse_indexprices_month_page(html):
    b = BeautifulSoup(html)
    t = b.select("div.abstand table.line")[0]
    data = []
    errors = []
    data_tmp = []
    for rows in t.find_all("tr")[1:-1]:
        for i,col in enumerate(rows.find_all("td")):
            if i == 6:
                date_tmp = None
                break
            if i == 5:
                continue 
            if i == 0:
                date_tmp = col.get_text()
                if len(date_tmp.split(".")) != 3:
                    errors.append("%s is not a date" % date_tmp)
                    break
                data_tmp.append(date_tmp)
            else:
                tmp = clean_parsed_numbr(col.get_text())
                if len(tmp) == 0:
                    errors.append("error with value %s" % tmp)
                    break
                try:
                    f = make_float(tmp)
                except ValueError:
                    f = -1
                    errors.append("%s is not a numbr" % tmp)
                data_tmp.append(f)

        data.append(tuple(data_tmp))
        data_tmp = []

    return ParseResult(data=tuple(data), errors=errors)
