from bs4 import BeautifulSoup
from ..utils import is_isin
from .utils import ParseResult

def parse_index_page(html):
    b = BeautifulSoup(html)
    tmp = b.select(".tableQuotes")[0]
    stock_table = tmp.select(".content")[0].find("table")
    stocks = []
    errors = []
    for rows in stock_table.find_all("tr"):
        for c in rows.find_all("td")[:1]:
            name = c.a.get_text().strip()
            isin = c.span.get_text().strip()
            if is_isin(isin):
                stocks.append((isin, name))
            else:
                errors.append("%s is not a isin!" % isin)
    return ParseResult(errors=errors, data=tuple(stocks))

def parse_dividend_page(html):
    b = BeautifulSoup(html)
    dividend_table = b.select(".table_quotes")[0].find("table")
    dividends_tmp = {}
    errors = []
    year = ""
    for rows in dividend_table.find_all("tr"):
        for i,c in enumerate(rows.find_all("td")):
            if i == 0:
                d = c.get_text()
                year = d.split(".")[2]
                if year not in dividends_tmp:
                    dividends_tmp[year] = 0
            if i == 3:
                d = c.get_text()
                d = float(d.split(" ")[0].replace(",", "."))
                if dividends_tmp[year] > 0:
                    dividends_tmp[year] += d
                    errors.append("Year %s exists more then once!" % str(int(year)-1))
                else:
                    dividends_tmp[year] = d

    dividends_sort = sorted(dividends_tmp.items(),
                            key=lambda x: x[0],
                            reverse=True)
    dividends = ((int(year)-1, money) 
                    for year,money in dividends_sort)
    return ParseResult(errors=errors, data=tuple(dividends))

def parse_appointments_page(html):
    errors = []
    appointments = []
    appointment_typ = {"Jahresabschluss": 1, "Quartalszahlen": 2}
    b = BeautifulSoup(html)
    d = b.select(".content_box .content table")
    if len(d) < 2:
        errors.append("couldn't find appointments table")

    for rows in d[2].find_all("tr"):
        for i,c in enumerate(rows.find_all("td")):
            if i == 0:
                t = c.get_text().strip()
                if t not in appointment_typ:
                    break
                t = appointment_typ[t]
            if i == 2:
                d = c.get_text().strip()
                if len(d.split(".")) != 3:
                    errors.append("%s ist not a date" % d)
                    break
                appointments.append((t,d))

    return ParseResult(errors=errors, data=tuple(appointments))
