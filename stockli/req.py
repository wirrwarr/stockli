import time
import calendar
from .utils import loop_process_pages, RequestTaskData, \
                    make_stock_url_ready, MemoryLogger, DataQueue, \
                    make_index_url_ready, make_date_object, \
                    request_urls
from .handlers import handler_stocks_from_index_page, \
                      handler_dividend_page, \
                      handler_keyfigures_page, \
                      handler_appointments_page, \
                      handler_stockprices_month_page, \
                      handler_indexprices_month_page, \
                      handler_stock_quantity_page

def request_stocks_from_index(indizes):
    url = request_urls["indices"]
    mlogger = MemoryLogger("stocks_from_index")
    dqueue = DataQueue("stocks_from_index")
    tasks = []
    for i in indizes:
        page_handler = handler_stocks_from_index_page(i, dqueue, mlogger)
        task = RequestTaskData(url=url % make_index_url_ready(i),
                handler=page_handler,
                logger=mlogger)
        tasks.append(task)

    loop_process_pages(tasks)

    return (dqueue, mlogger)

def request_dividends(stocks):
    url = request_urls["dividents"]
    mlogger = MemoryLogger("stock_dividends")
    dqueue = DataQueue("stock_dividends")
    c = len(stocks) 
    results = {}
    tasks = []
    for i in range(0,c,20):
        tmp_stocks = stocks[i:i+20]
        for s in tmp_stocks:
            page_handler = handler_dividend_page(s, dqueue, mlogger)
            task = RequestTaskData(url=url % make_stock_url_ready(s.name),
                         handler=page_handler,
                         logger=mlogger)
            tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)

    return (dqueue, mlogger)

def request_appointments(stocks):
    url = request_urls["appointments"]
    mlogger = MemoryLogger("stock_appointments")
    dqueue = DataQueue("stock_appointments")
    c = len(stocks) 
    results = {}
    tasks = []
    for i in range(0,c,20):
        tmp_stocks = stocks[i:i+20]
        for s in tmp_stocks:
            page_handler = handler_appointments_page(s.isin, dqueue, mlogger)
            task = RequestTaskData(url=url % make_stock_url_ready(s.name),
                         handler=page_handler,
                         logger=mlogger)
            tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)
        tasks = []

    return (dqueue, mlogger)

def request_keyfigures(stocks):
    url = request_urls["keyfigures"]
    mlogger = MemoryLogger("stock_keyfigures")
    dqueue = DataQueue("stock_keyfigures")
    c = len(stocks) 
    results = {}
    tasks = []
    for i in range(0,c,20):
        tmp_stocks = stocks[i:i+20]
        for s in tmp_stocks:
            page_handler = handler_keyfigures_page(s.isin, dqueue, mlogger)
            task = RequestTaskData(url=url % s.isin,
                         handler=page_handler,
                         logger=mlogger)
            tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)
        tasks = []
    return (dqueue, mlogger)

def request_stock_quantity(stocks):
    url = request_urls["stock_quantity"]
    mlogger = MemoryLogger("stock_quantity")
    dqueue = DataQueue("stock_quantity")
    c = len(stocks) 
    results = {}
    tasks = []
    for i in range(0,c,20):
        tmp_stocks = stocks[i:i+20]
        for s in tmp_stocks:
            page_handler = handler_stock_quantity_page(s.isin, dqueue, mlogger)
            task = RequestTaskData(url=url % (s.name, s.isin),
                         handler=page_handler,
                         logger=mlogger)
            tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)
        tasks = []
    return (dqueue, mlogger)

def request_stockprices_month(stocks, market, dates):
    url = request_urls["stockprices_month"]
    mlogger = MemoryLogger("stock_prices")
    dqueue = DataQueue("stock_prices")
    c = len(stocks) 
    ldates = len(dates)
    stocks_per_loop = int(40/ldates)
    results = {}
    tasks = []

    for i in range(0,c,stocks_per_loop):
        tmp_stocks = stocks[i:i+stocks_per_loop]
        for s in tmp_stocks:
            for d in dates:
                dtmp = make_date_object(d)
                handler = handler_stockprices_month_page(s.isin, market.pk, 
                                                    dqueue, mlogger)
                task = RequestTaskData( \
                              url=url % (s.isin, dtmp),
                              handler=handler,
                              logger=mlogger)
                tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)
        tasks = []
    return (dqueue, mlogger)

def request_indexprices_month(indices, dates):
    url = request_urls["indexprices_month"]
    mlogger = MemoryLogger("index_prices")
    dqueue = DataQueue("index_prices")
    c = len(indices) 
    ldates = len(dates)
    indices_per_loop = int(40/ldates)
    results = {}
    tasks = []
    for i in range(0,c,indices_per_loop):
        tmp_indices = indices[i:i+indices_per_loop]
        for ind in tmp_indices:
            for d in dates:
                dtmp = make_date_object(d)
                handler = handler_indexprices_month_page(ind.pk,
                                                    dqueue, mlogger)
                task = RequestTaskData( \
                              url=url % (ind.name, dtmp),
                              handler=handler,
                              logger=mlogger)
                tasks.append(task)

        loop_process_pages(tasks)
        time.sleep(0.5)
        tasks = []
    return (dqueue, mlogger)
