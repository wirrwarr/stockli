import sys
from .parser.finanzen import parse_index_page, \
                             parse_dividend_page,\
                             parse_appointments_page
from .parser.ariva import parse_stockprices_month_page, \
                        parse_indexprices_month_page
from .parser.onvista import parse_keyfigures_page_2, \
                            parse_stock_quantity_page
from .parser.wallstreetonline import parse_keyfigures_page
from .save import save_stocks_from_index, save_dividends, \
                  save_keyfigures, save_appointments, \
                  save_stockprices_month, save_indexprices_month, \
                  save_keyfigures_2, save_stock_quantity

def handler_stocks_from_index_page(index_name, dataqueue, logger):
    def handler(html):
        parser_result = parse_index_page(html)
        tmp = [index_name, []]
        for o in parser_result.data:
            tmp[1].append(o)
        dataqueue.add(tmp)
        for m in parser_result.errors:
            logger.error("%s: %s" %(index, m))
        save_stocks_from_index(index_name, parser_result, logger)

    return handler

def handler_dividend_page(isin, dataqueue, logger):
    def handler(html):
        parser_result = parse_dividend_page(html)
        tmp = [isin, []]
        for o in parser_result.data:
            tmp[1].append(o)
        dataqueue.add(tmp)
        for m in parser_result.errors:
            logger.error("%s: %s" %(isin, m))
        save_dividends(isin, parser_result, logger)

    return handler

def handler_keyfigures_page(isin, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_keyfigures_page(html)
            tmp = [isin, parser_result.data]
            dataqueue.add(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(isin, m))
            try:
                save_keyfigures(isin, parser_result, logger)
            except:
                logger.error("handler_keyfigures_page:%s: %s" % (isin, sys.exc_info()))
        except:
            logger.error("handler_keyfigures_page:%s: %s" % (isin, sys.exc_info()))

    return handler

def get_traceback():
    import sys, traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print("*** print_tb:")
    traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
    print("*** print_exception:")
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=sys.stdout)

def handler_keyfigures_page_2(isin, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_keyfigures_page_2(html)
            tmp = [isin, parser_result.data]
            dataqueue.add(tmp)
            #print(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(isin, m))
            try:
                save_keyfigures_2(isin, parser_result, logger)
            except:
                logger.error("handler_keyfigures_page_2:save:%s: %s" % (isin, sys.exc_info()))
        except:
            logger.error("handler_keyfigures_page_2:%s: %s" % (isin, sys.exc_info()))

    return handler

def handler_appointments_page(isin, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_appointments_page(html)
            if len(parser_result.data) == 0:
                logger.info("%s can't find appointments" % isin)
            tmp = [isin, parser_result.data]
            dataqueue.add(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(isin, m))
            try:
                save_appointments(isin, parser_result, logger)
            except:
                logger.error("handler_appointments_page:%s: %s" % (isin, sys.exc_info()))
        except:
            logger.error("handler_appointments_page:%s: %s" % (isin, sys.exc_info()))

    return handler

def handler_stock_quantity_page(isin, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_stock_quantity_page(html)
            if len(parser_result.data) == 0:
                logger.info("%s can't find stock quantity" % isin)
            tmp = [isin, parser_result.data[0]]
            #print(tmp)
            dataqueue.add(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(isin, m))
            try:
                save_stock_quantity(isin, parser_result, logger)
            except:
                logger.error("handler_stock_quantity_page:%s: %s" % (isin, sys.exc_info()))
        except:
            logger.error("handler_stock_quantity_page:%s: %s" % (isin, sys.exc_info()))

    return handler

def handler_stockprices_month_page(isin, market_id, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_stockprices_month_page(html)
            if len(parser_result.data) == 0:
                logger.info("%s %s can't find stockprices" % (isin, market_id))
            tmp = [isin, parser_result.data]
            dataqueue.add(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(isin, m))
            try:
                save_stockprices_month(isin, market_id, parser_result, logger)
            except:
                logger.error("handler_stockprices_month_page:save:%s: %s" % (isin, sys.exc_info()))
        except:
            logger.error("handler_stockprices_month_page:parse:%s: %s" % (isin, sys.exc_info()))

    return handler

def handler_indexprices_month_page(index_id, dataqueue, logger):
    def handler(html):
        try:
            parser_result = parse_indexprices_month_page(html)
            if len(parser_result.data) == 0:
                logger.info("%s can't find index prices" % (index_id))
            tmp = [index_id, parser_result.data]
            dataqueue.add(tmp)
            for m in parser_result.errors:
                logger.error("%s: %s" %(index_id, m))
            try:
                save_indexprices_month(index_id, parser_result, logger)
            except:
                logger.error("handler_indexprices_month_page:%s: %s" % (index_id, sys.exc_info()))
        except:
            logger.error("handler_indexprices_monht_page:%s: %s" % (index_id, sys.exc_info()))

    return handler
