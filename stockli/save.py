from datetime import datetime
from datetime import date
from .models import Stock, Index, Dividend, \
                    KeyFigures, Appointment, \
                    Market, StockPrice, IndexPrice, \
                    KeyFigures2, StockQuantity
from .utils import insert_values, make_date_object

def save_stocks_from_index(index, result, logger):
    index = Index.objects.filter(name=index)[0]
    index_stocks = index.stock_set.all()
    index_stocks_isin = [s.isin for s in index_stocks]
    cur_stocks = Stock.objects.all()
    cur_stocks_isin = [s.isin for s in cur_stocks]
    stocks = [Stock(isin=s[0], name=s[1]) 
                for s in result.data
                    if s[0] not in cur_stocks_isin]
    stocks_already_exists = [Stock(isin=s[0], name=s[1]) for s in result.data
                                if s[0] not in index_stocks_isin
                                if s[0] not in [x.isin for x in stocks]] 
    Stock.objects.bulk_create(stocks)
    stocks_for_index = stocks + stocks_already_exists
    if len(stocks_for_index) != 0:
        insert_values([(s.pk, index.pk) for s in stocks_for_index], \
                      ("stock_id", "index_id"), "stockli_stock_indizes") 

def save_dividends(isin, result, logger):
    stock = Stock(isin=isin, name="")
    cur_dividends = stock.dividend_set.all().order_by("year")
    cur_dividend_years = [s.year for s in cur_dividends]
    dividends = [Dividend(year=r[0], money=r[1], stock=stock)
                    for r in result.data
                        if r[0] not in cur_dividend_years]
    Dividend.objects.bulk_create(dividends)

def save_stock_quantity(isin, result, logger):
    stock = Stock(isin=isin, name="")
    cur_stockquantity = stock.stockquantity_set.all().order_by("date")
    cur_stockquantity_dates = [s.date for s in cur_stockquantity]
    d = datetime.today()
    d = date(year=d.year, month=d.month, day=d.day)
    for q in result.data:
        if d not in cur_stockquantity_dates:
            StockQuantity.objects.create(stock=stock, date=d, quantity=q)

def save_keyfigures(isin, result, logger):
    stock = Stock(isin=isin, name="")
    cur_keyfigures = stock.keyfigures_set.all().order_by("year")
    cur_keyfigures_years = [k.year for k in cur_keyfigures]
    keyfigures = [KeyFigures(year=y, stock=stock,\
                             per=d["per"], pcr=d["pcr"], \
                             psr=d["psr"], pbr=d["pbr"], \
                             return_on_sales=d["return_on_sales"], \
                             roi=d["roi"], equity_ratio=d["equity_ratio"], \
                             debt_ratio=d["debt_ratio"], \
                             liquidity=d["liquidity"]) \
                    for y,d in result.data.items()
                        if y not in cur_keyfigures_years]
    KeyFigures.objects.bulk_create(keyfigures)

def save_keyfigures_2(isin, result, logger):
    stock = Stock(isin=isin, name="")
    cur_keyfigures = stock.keyfigures2_set.all().order_by("year")
    cur_keyfigures_years = [k.year for k in cur_keyfigures]
    new_result = {}
    for y,d in result.data.items():
        try:
            y.index("e")
        except ValueError:
            new_result[int(y)] = d.copy() 

    keyfigures = [KeyFigures2(year=y, stock=stock,\
                             per=d["per"], ebit_marge=d["ebit_marge"], \
                             roe=d["roe"], equity_ratio=d["equity_ratio"]) \
                    for y,d in new_result.items()
                        if y not in cur_keyfigures_years]
    KeyFigures2.objects.bulk_create(keyfigures)

def save_appointments(isin, result, logger):
    stock = Stock(isin=isin, name="")
    atmp = []
    cur_appointments = stock.appointment_set.all()
    cur_appointments = [[a.typ,a.date] for a in cur_appointments]
    # Finde doppelte Einträge Eingabe fehler auf finanze.net
    seen = set()
    uniq = [x for x in result.data if x not in seen and not seen.add(x)]
    for t,d in uniq:
        d = d.split(".")
        d = date(year=int(d[2])+2000, month=int(d[1]), day=int(d[0]))
        atmp.append((t,d))
    appoints = [Appointment(typ=t, date=d, stock=stock)
                    for t,d in atmp
                        if [t,d] not in cur_appointments]
    Appointment.objects.bulk_create(appoints)

def save_stockprices_month(isin, market_id, result, logger):
    stock = Stock(isin=isin, name="")
    market = Market(id=market_id, name="")
   
    tmp = sorted(result.data, key=lambda x: x[0])
    oldest_date = make_date_object(tmp[0][0])
    newest_date = make_date_object(tmp[-1][0])
    cur_prices_month = stock.stockprice_set.filter(date__range=(oldest_date, newest_date))
    cur_prices_month = [[p.date, p.market, p.stock] for p in cur_prices_month]
    prices_to_save = [StockPrice(stock=stock, market=market, \
                                 date=make_date_object(p[0]), \
                                 start=p[1], high=p[2], low=p[3], \
                                 end=p[4], pieces=p[5]) 
                        for p in result.data
                        if [make_date_object(p[0]), \
                            market, stock] not in cur_prices_month]

    StockPrice.objects.bulk_create(prices_to_save)

def save_indexprices_month(index_id, result, logger):
    index = Index(id=index_id, name="")
   
    tmp = sorted(result.data, key=lambda x: x[0])
    oldest_date = make_date_object(tmp[0][0])
    newest_date = make_date_object(tmp[-1][0])
    cur_prices_month = index.indexprice_set.filter(date__range=(oldest_date, newest_date))
    cur_prices_month = [[p.date, p.index] for p in cur_prices_month]
    prices_to_save = [IndexPrice(index=index, date=make_date_object(p[0]), \
                                 start=p[1], high=p[2], low=p[3], end=p[4]) 
                        for p in result.data
                        if [make_date_object(p[0]), index] \
                            not in cur_prices_month]

    IndexPrice.objects.bulk_create(prices_to_save)
