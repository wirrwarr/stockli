import calendar
from datetime import datetime
from django.conf import settings
from django.shortcuts import render
from django.core.context_processors import csrf
from .req import request_stocks_from_index, request_dividends, \
                 request_keyfigures, request_appointments, \
                 request_stockprices_month, \
                 request_indexprices_month, \
                 request_stock_quantity
from .utils import query_indices_stocks, make_date_object
from .models import Index, Stock, Market
from .forms import IndexForm
from .analysis import loop_stock_analysis_large_cap, \
                      loop_stock_analysis_large_cap_2

def view_stocks_from_index(request):
    if request.method == "POST":
        index_names = []
        tmp = request.POST.getlist("name")
        if len(tmp) > 0:
            for i in tmp:
                i = Index.objects.get(pk=i)
                index_names.append(i.name)
        data, logger = request_stocks_from_index(index_names)

        context = {"index_form": IndexForm,
                   "indizes": data.objects(),
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/stocks_from_index.html', context)

    context = {"index_form": IndexForm}
    context.update(csrf(request))
    return render(request, 'stockli/stocks_from_index.html', context)

def view_dividends(request):
    stocks = Stock.objects.all().order_by("name")
    if request.method == "POST":
        selected_isins = request.POST.getlist("stocks")
        selected_stocks = [s for s in stocks if s.isin in selected_isins]
        data, logger = request_dividends(selected_stocks)
        context = {"stocks": stocks,
                   "dividends": data.objects(),
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/dividends.html', context)
    if request.method == "GET": 
        context = {"stocks": stocks}
        context.update(csrf(request))
        return render(request, 'stockli/dividends.html', context)

def view_stock_quantity(request):
    stocks = Stock.objects.all().order_by("name")
    if request.method == "POST":
        selected_isins = request.POST.getlist("stocks")
        selected_stocks = [s for s in stocks if s.isin in selected_isins]
        data, logger = request_stock_quantity(selected_stocks)
        context = {"stocks": stocks,
                   "stock_quantity": data.objects(),
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/stock_quantity.html', context)
    if request.method == "GET": 
        context = {"stocks": stocks}
        context.update(csrf(request))
        return render(request, 'stockli/stock_quantity.html', context)

def view_keyfigures(request):
    stocks = Stock.objects.all().order_by("name")
    if request.method == "POST":
        selected_isins = request.POST.getlist("stocks")
        selected_stocks = [s for s in stocks if s.isin in selected_isins]
        data, logger = request_keyfigures(selected_stocks)
        context = {"stocks": stocks,
                   "dividends": data.objects(),
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/keyfigures.html', context)
    if request.method == "GET": 
        context = {"stocks": stocks}
        context.update(csrf(request))
        return render(request, 'stockli/keyfigures.html', context)

def view_appointments(request):
    stocks = Stock.objects.all().order_by("name")
    if request.method == "POST":
        selected_isins = request.POST.getlist("stocks")
        selected_stocks = [s for s in stocks if s.isin in selected_isins]
        data, logger = request_appointments(selected_stocks)
        context = {"stocks": stocks,
                   "dividends": data.objects(),
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/appointments.html', context)
    if request.method == "GET": 
        context = {"stocks": stocks}
        context.update(csrf(request))
        return render(request, 'stockli/appointments.html', context)

def view_stockprices_month(request):
    stocks = Stock.objects.all().order_by("name")

    months = []
    t = datetime.today()
    mtmp = t.month
    ytmp = t.year
    for x in range(1, 12+1):
        lastday = calendar.monthrange(ytmp, mtmp)[1]
        months.append("%s.%s.%s" % (lastday, mtmp, ytmp))

        if mtmp == 1: 
            ytmp = ytmp - 1
            mtmp = 12
        else:
            mtmp = mtmp -1

    if request.method == "POST":
        selected_isins = request.POST.getlist("stocks")
        selected_months = request.POST.getlist("months")
        selected_stocks = [s for s in stocks if s.isin in selected_isins]
        market = Market(id=6, name="")
        data, logger = request_stockprices_month(selected_stocks, \
                                            market, selected_months)
        context = {"stocks": stocks,
                   "months": months,
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/stockprices.html', context)
    if request.method == "GET": 
        context = {"stocks": stocks, "months": months}
        context.update(csrf(request))
        return render(request, 'stockli/stockprices.html', context)

def view_indexprices_month(request):
    indices = Index.objects.all().order_by("name")

    months = []
    t = datetime.today()
    mtmp = t.month
    ytmp = t.year
    for x in range(1, 12+1):
        lastday = calendar.monthrange(ytmp, mtmp)[1]
        months.append("%s.%s.%s" % (lastday, mtmp, ytmp))

        if mtmp == 1: 
            ytmp = ytmp - 1
            mtmp = 12
        else:
            mtmp = mtmp -1

    if request.method == "POST":
        selected_indices = request.POST.getlist("indices")
        selected_months = request.POST.getlist("months")
        selected_indices = [i for i in indices 
                                if str(i.pk) in selected_indices]
        data, logger = request_indexprices_month(selected_indices, \
                                                 selected_months)
        context = {"indices": indices,
                   "months": months,
                   "logger": logger}
        context.update(csrf(request))
        return render(request, 'stockli/indexprices.html', context)
    if request.method == "GET": 
        context = {"indices": indices, "months": months}
        context.update(csrf(request))
        return render(request, 'stockli/indexprices.html', context)

def view_stock_analysis(request):
    #logger = ""
    #context = {"analysis_result": analysis_result, "logger": logger}
    today = datetime.today()
    all_stocks = Stock.objects.all().order_by("name")
    indices = Index.objects.all().order_by("name")
    if request.method == "GET":
        context = {"stocks": all_stocks,
                "indices": indices,
                "date": today.strftime("%d.%m.%Y")}
        context.update(csrf(request))
        return render(request, 'stockli/stock_analysis.html', context)

    if request.method == "POST":
        selected_indices = request.POST.getlist("indices")
        selected_isins = request.POST.getlist("stocks")
        date = today

        if "date" in request.POST:
            date = make_date_object(request.POST["date"])

        selected_stocks = []
        if len(selected_isins) != 0:
            selected_stocks = [s for s in all_stocks if s.isin in selected_isins]

        indices_stocks = []
        if len(selected_indices) != 0:
            selected_indices = [i for i in indices 
                                    if str(i.pk) in selected_indices]
            indices_stocks = query_indices_stocks(selected_indices)

        stocks = selected_stocks + list(indices_stocks)
        analysis_results, logger = loop_stock_analysis_large_cap(stocks, Index(id=1, name="DAX"), date)
        context = {"stocks": all_stocks,
                "indices": indices,
                "analysis_results": list(reversed(sorted(analysis_results.objects(), key=lambda x: x.points()))),
                "logger": logger,
                "date": date.strftime("%d.%m.%Y")}
        context.update(csrf(request))
        return render(request, 'stockli/stock_analysis.html', context)

def view_stock_analysis_2(request):
    #logger = ""
    #context = {"analysis_result": analysis_result, "logger": logger}
    today = datetime.today()
    all_stocks = Stock.objects.all().order_by("name")
    indices = Index.objects.all().order_by("name")
    if request.method == "GET":
        context = {"stocks": all_stocks,
                "indices": indices,
                "date": today.strftime("%d.%m.%Y")}
        context.update(csrf(request))
        return render(request, 'stockli/stock_analysis_2.html', context)

    if request.method == "POST":
        selected_indices = request.POST.getlist("indices")
        selected_isins = request.POST.getlist("stocks")
        date = today

        if "date" in request.POST:
            date = make_date_object(request.POST["date"])

        selected_stocks = []
        if len(selected_isins) != 0:
            selected_stocks = [s for s in all_stocks if s.isin in selected_isins]

        indices_stocks = []
        if len(selected_indices) != 0:
            selected_indices = [i for i in indices 
                                    if str(i.pk) in selected_indices]
            indices_stocks = query_indices_stocks(selected_indices)

        stocks = selected_stocks + list(indices_stocks)
        analysis_return, logger = loop_stock_analysis_large_cap_2(stocks, Index(id=1, name="DAX"), date)
        context = {"stocks": all_stocks,
                "indices": indices,
                "analysis_return": list(reversed(sorted(analysis_return.objects(), key=lambda x: x.analysis_result.points()))),
                "logger": logger,
                "date": date.strftime("%d.%m.%Y")}
        context.update(csrf(request))
        return render(request, 'stockli/stock_analysis_2.html', context)
