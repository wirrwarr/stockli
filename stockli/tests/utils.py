import os
from datetime import date
from django.test import TestCase
from ..utils import is_isin, replace_alpha, \
                make_stock_url_ready, make_float, \
                make_index_url_ready, clean_parsed_numbr, \
                make_date_object, DateMod, \
                find_pre_post_days, find_last_weekday_month, \
                query_indices_stocks, make_stock_onvistaurl_ready, \
                query_stock_indices, is_elected
from ..models import Index, Stock

TEST_DIR = os.path.dirname(__file__)

class UtilsTestCase(TestCase):

    def test_is_isin_error_length(self):
        self.assertEqual(False, is_isin("E000BAY0017"))

    def test_is_isin_error_lastpos_alpha(self):
        self.assertEqual(False, is_isin("DE000BAY001A"))

    def test_is_isin_error_lastpos_wrong_checksum(self):
        self.assertEqual(False, is_isin("DE000BAY0019"))

    def test_is_isin_error_firstpos(self):
        self.assertEqual(False, is_isin("1E000BAY0017"))
        self.assertEqual(False, is_isin("D2000BAY0017"))

    def test_is_isin(self):
        self.assertEqual(True, is_isin("DE000BAY0017"))

    def test_check_luhn(self):
        self.assertEqual("1011121", replace_alpha("aBc1"))

    def test_make_stock_url_ready(self):
        self.assertEqual("alstria_office_REIT-AG", make_stock_url_ready("alstria office REIT-AG")) 

    def test_make_stock_onvistaurl_ready(self):
        self.assertEqual("alstria-office-REIT-AG", make_stock_onvistaurl_ready("alstria office REIT-AG")) 

    def test_make_stock_url_ready(self):
        self.assertEqual("S&P_500", make_index_url_ready("S&P 500")) 

    def test_make_float(self):
        self.assertEqual(-5523.42, make_float("-5.523,42"))
        self.assertEqual(5523.42, make_float("5.523,42"))
        self.assertEqual(42, make_float("42"))
        self.assertEqual(-0.9, make_float("-0,9"))
        self.assertEqual(0.98, make_float("0,98"))

    def test_clean_parsed_numbr(self):
        self.assertEqual("-0.98", clean_parsed_numbr("$-0.98€  % /&("))

    def test_make_date_object(self):
        self.assertEqual(date(day=1, month=1, year=2001), make_date_object("01.01.01"))
        self.assertEqual(date(day=1, month=1, year=2001), make_date_object("1.1.01"))
        self.assertEqual(date(day=1, month=1, year=2001), make_date_object("1.01.2001"))

    def test_add_months(self):
        d = DateMod(year=2000, month=2, day=1)
        self.assertEqual(date(day=1, month=3, year=2000), d.mod_month(1))

        tdate = date(year=2000, month=2, day=1)
        d = DateMod(date_obj=tdate)
        self.assertEqual(date(day=1, month=3, year=2000), d.mod_month(1))

    def test_sub_months(self):
        d = DateMod(year=2000, month=1, day=1)
        self.assertEqual(date(day=1, month=12, year=1999), d.mod_month(-1))

    def test_sub_months_s(self):
        d = DateMod(year=2012, month=3, day=30)
        self.assertEqual(date(day=29, month=2, year=2012), d.mod_month(-1))

    def test_add_year(self):
        d = DateMod(year=2000, month=1, day=1)
        self.assertEqual(date(day=1, month=1, year=2001), d.mod_year(1))

    def test_sub_year(self):
        d = DateMod(year=2000, month=1, day=1)
        self.assertEqual(date(day=1, month=1, year=1999), d.mod_year(-1))

    def test_find_pre_post_days(self):
        self.assertEqual((date(day=6, month=11, year=2014), date(day=10, month=11, year=2014)), find_pre_post_days(date(day=7, month=11, year=2014)))
        self.assertEqual((date(day=7, month=11, year=2014), date(day=11, month=11, year=2014)), find_pre_post_days(date(day=10, month=11, year=2014)))
        self.assertEqual((date(day=4, month=11, year=2014), date(day=6, month=11, year=2014)), find_pre_post_days(date(day=5, month=11, year=2014)))

    def test_find_last_weekday_month(self):
        self.assertEqual(date(day=29, month=2, year=2012), find_last_weekday_month(2, 2012))
        self.assertEqual(date(day=30, month=5, year=2014), find_last_weekday_month(5, 2014))
        self.assertEqual(date(day=31, month=3, year=2014), find_last_weekday_month(3, 2014))

    def test_query_indices_stocks(self):
        dax = Index.objects.create(name="DAX")
        mdax = Index.objects.create(name="MDAX")
        sdax = Index.objects.create(name="SDAX")
        
        dax_stocks = []
        mdax_stocks = []
        sdax_stocks = []
        for i in range(10):
            t = Stock.objects.create(isin=str(i), name=str(i))
            t.indizes.add(dax)
            dax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+100), name=str(i+100))
            t.indizes.add(mdax)
            mdax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+200), name=str(i+200))
            t.indizes.add(sdax)
            sdax_stocks.append(t)
        expected_result = tuple(sorted([s for s in dax_stocks] + \
                     [s for s in mdax_stocks], key=lambda x: x.name))
        result = query_indices_stocks([dax, mdax])
        self.assertEqual(len(expected_result), len(result))
        for i,r in enumerate(result):
            self.assertEqual([expected_result[i].name, expected_result[i].isin], [r.name, r.isin])

    def test_query_stocks_indices_all_stocks(self):
        dax = Index.objects.create(name="DAX", id=4)
        mdax = Index.objects.create(name="MDAX", id=5)
        
        dax_stocks = []
        mdax_stocks = []
        sdax_stocks = []
        none_stocks = []
        for i in range(2):
            t = Stock.objects.create(isin=str(i+100), name=str(i+100))
            t.indizes.add(dax)
            dax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+200), name=str(i+200))
            t.indizes.add(mdax)
            mdax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+300), name=str(i+300))
            none_stocks.append(t)


        expected_result = {"100": {"name": "100", "index": "DAX", "index_id": 4},
                            "101": {"name": "101", "index": "DAX", "index_id": 4},
                            "200": {"name": "200", "index": "MDAX", "index_id": 5},
                            "201": {"name": "201", "index": "MDAX", "index_id": 5},
                            "300": {"name": "300", "index": None, "index_id": None},
                            "301": {"name": "301", "index": None, "index_id": None}}

        result = query_stock_indices()
        self.assertEqual(len(expected_result), len(result))
        for k,v in result.items():
            self.assertEqual([expected_result[k]["name"],
                              expected_result[k]["index"],
                              expected_result[k]["index_id"]], 
                             [result[k]["name"], result[k]["index"], result[k]["index_id"]])

    def test_query_stocks_indices_special_stocks(self):
        dax = Index.objects.create(name="DAX", id=6)
        
        dax_stocks = []
        none_stocks = []
        for i in range(2):
            t = Stock.objects.create(isin=str(i+100), name=str(i+100))
            t.indizes.add(dax)
            dax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+300), name=str(i+300))
            none_stocks.append(t)


        expected_result = {"100": {"name": "100", "index": "DAX", "index_id": 6},
                           "301": {"name": "301", "index": None, "index_id": None}}

        result = query_stock_indices([dax_stocks[0], none_stocks[1]])
        self.assertEqual(len(expected_result), len(result))
        for k,v in result.items():
            self.assertEqual([expected_result[k]["name"],
                              expected_result[k]["index"],
                              expected_result[k]["index_id"]], 
                             [result[k]["name"], result[k]["index"], result[k]["index_id"]])
            
    def test_is_stock_elected(self):
        dax = Index.objects.create(name="DAX", id=7)
        
        dax_stocks = []
        mdax_stocks = []
        sdax_stocks = []
        none_stocks = []
        for i in range(2):
            t = Stock.objects.create(isin=str(i+100), name=str(i+100))
            t.indizes.add(dax)
            dax_stocks.append(t)
            t = Stock.objects.create(isin=str(i+300), name=str(i+300))
            none_stocks.append(t)


        expected_result = {"100": {"name": "100", "index": "DAX", "index_id": 7},
                           "301": {"name": "301", "index": None, "index_id": None}}

        result = query_stock_indices([dax_stocks[0], none_stocks[1]])
        self.assertEqual(is_elected(dax_stocks[0], 3, result), False)
        self.assertEqual(is_elected(dax_stocks[1], 6, result), True)


