from datetime import datetime
from datetime import date
from django.test import TestCase
from ..models import Index, Stock, Dividend, KeyFigures, \
                    Appointment, Market, StockPrice, IndexPrice, \
                    KeyFigures2
from ..parser.utils import ParseResult
from ..save import save_stocks_from_index, save_dividends, \
                    save_keyfigures, save_appointments, \
                    save_stockprices_month, save_indexprices_month, \
                    save_keyfigures_2, save_stock_quantity
from ..utils import MemoryLogger, DataQueue, make_date_object

dividend_result = (
            (2014,0.20),
            (2013,0.15),
            (2012,0.10),
            (2011,0.10),
            (2010,0.0),
            (2009,0.0),
            (2008,0.0),
            (2007,0.0),
            (2006,0.0),
            (2005,0.0),
            (2004,0.0),
            (2003,0.0)
        )

class SaveRequestDataTestCase(TestCase):
    def test_save_stocks_in_db(self):
        mlogger = MemoryLogger("")
        i = Index.objects.create(name="DAX")
        save_stocks_from_index("DAX", ParseResult(data=[["US90130A1016", "test"], ["DE0008404005", "test2"]], errors=[]), mlogger)
        stock = Stock.objects.get(pk="US90130A1016")
        t = sorted(Index.objects.get(pk=i.pk).stock_set.all(), key=lambda x: x.name)
        self.assertEqual([t[0].name, t[0].isin], [stock.name, stock.isin])
        self.assertEqual(["test", "US90130A1016"], [stock.name, stock.isin])

    def test_save_dividend_in_db(self):
        mlogger = MemoryLogger("")
        s = Stock.objects.create(name="CENTROTEC Sustainable", isin="DE0005407506")
        save_dividends("DE0005407506", ParseResult(data=dividend_result[0:3], errors=[]), mlogger)
        d = []
        dividend_test_list = [Dividend(year=v[0], money=v[1], stock=s) 
                                for v in dividend_result[0:3]]
        for i,d in enumerate(s.dividend_set.all().order_by("-year")):
            self.assertEqual((dividend_test_list[i].year, dividend_test_list[i].money),(d.year, d.money))

    def test_save_dividend_in_db_check_if_year_already_exists(self):
        mlogger = MemoryLogger("")
        dividend_parsed = (
                    (2014,0.20),
                    (2013,0.15),
                    (2012,1.10),
                    (2011,0.00),
                )
        dividend_stored = (
                    (2014,0.20),
                    (2013,0.15),
                    (2012,0.10),
                    (2011,1.00),
                )
        s = Stock.objects.create(name="CENTROTEC Sustainable", isin="DE0005407506")
        d = Dividend.objects.create(year=2011, money=1.0, stock=s)
        d = Dividend.objects.create(year=2012, money=0.1, stock=s)
        save_dividends("DE0005407506", ParseResult(data=dividend_parsed, errors=[]), mlogger)
        d = []
        dividend_test_list = [Dividend(year=v[0], money=v[1], stock=s) 
                                for v in dividend_stored]

        self.assertEqual(len(dividend_stored), s.dividend_set.all().count())

        for i,d in enumerate(s.dividend_set.all().order_by("-year")):
            self.assertEqual((dividend_test_list[i].year, dividend_test_list[i].money),(d.year, d.money))

    def test_save_keyfigures_in_db_check_if_year_already_exists(self):
        mlogger = MemoryLogger("")
        parsed_keyfigures = {2009: {"per": 11.4,
                                    "pcr": 1.14,
                                    "psr": 0.63,
                                    "pbr": 2.95,
                                    "return_on_sales": 5.23,
                                    "roi": 2.34,
                                    "equity_ratio": 9.41,
                                    "debt_ratio": 90.59,
                                    "liquidity": 52.13,
                                    },
                             2010: {"per": 15.20,
                                    "pcr": 3.06,
                                    "psr": 1.64,
                                    "pbr": 4.80,
                                    "return_on_sales": 10.42,
                                    "roi": 4.95,
                                    "equity_ratio": 16.24,
                                    "debt_ratio": 83.76,
                                    "liquidity": 62.87,
                                    },
                            }

        keyfigures_in_db = {2009: {"per": 11.2,
                                    "pcr": 1.16,
                                    "psr": 0.62,
                                    "pbr": 2.95,
                                    "return_on_sales": 5.23,
                                    "roi": 2.34,
                                    "equity_ratio": 9.41,
                                    "debt_ratio": 90.59,
                                    "liquidity": 52.13,
                                    },
                             2010: {"per": 15.20,
                                    "pcr": 3.06,
                                    "psr": 1.64,
                                    "pbr": 4.80,
                                    "return_on_sales": 10.42,
                                    "roi": 4.95,
                                    "equity_ratio": 16.24,
                                    "debt_ratio": 83.76,
                                    "liquidity": 62.87,
                                    },
                            }

        s = Stock.objects.create(name="ProSieben SAT.1 N", isin="DE000PSM7770")
        kf = KeyFigures.objects.create(per=11.2,\
                                    pcr=1.16,\
                                    psr=0.62,\
                                    pbr=2.95,\
                                    return_on_sales=5.23,\
                                    roi=2.34,\
                                    equity_ratio=9.41,\
                                    debt_ratio=90.59,\
                                    liquidity=52.13,
                                    year=2009,
                                    stock=s)
        save_keyfigures("DE000PSM7770", ParseResult(data=parsed_keyfigures, errors=[]), mlogger)

        self.assertEqual(2, s.keyfigures_set.all().count())

        for y,f in keyfigures_in_db.items():
            nkf = s.keyfigures_set.filter(year=y)[0]
            for k,v in f.items():
                self.assertEqual(v, getattr(nkf, k))

    def test_save_keyfigures2_in_db_check_if_year_already_exists(self):
        mlogger = MemoryLogger("")
        parsed_keyfigures = {"2011": {"per": 9.73,
                                    "ebit_marge": 34.54,
                                    "roe": 44.22,
                                    "equity_ratio": 28.63,
                                    },
                             "2010": {"per": 13.55,
                                    "ebit_marge": 21.62,
                                    "roe": 30.48,
                                    "equity_ratio": 16.24,
                                    },
                            }

        keyfigures_in_db = {2011: {"per": 9.73,
                                    "ebit_marge": 34.54,
                                    "roe": 44.22,
                                    "equity_ratio": 28.63,
                                    },
                             2010: {"per": 13.56,
                                    "ebit_marge": 21.64,
                                    "roe": 30.49,
                                    "equity_ratio": 16.25,
                                    },
                            }

        s = Stock.objects.create(name="ProSieben SAT.1 N", isin="DE000PSM7770")
        kf = KeyFigures2.objects.create(per=13.56,\
                                    ebit_marge=21.64,\
                                    roe=30.49,\
                                    equity_ratio=16.25,\
                                    year=2010,
                                    stock=s)
        save_keyfigures_2("DE000PSM7770", ParseResult(data=parsed_keyfigures, errors=[]), mlogger)

        self.assertEqual(2, s.keyfigures2_set.all().count())

        for y,f in keyfigures_in_db.items():
            nkf = s.keyfigures2_set.filter(year=y)[0]
            for k,v in f.items():
                self.assertEqual(v, getattr(nkf, k))

    def test_save_appointments_in_db(self):
        mlogger = MemoryLogger("")
        appo_res = ((2, "07.08.14"),
                    (2, "06.05.14"),
                    (1, "05.03.14"),
                    (2, "07.11.13"),
                    (2, "08.08.13"))
        appo_indb = ((2, date(year=2014, month=8, day=7)),
                    (2, date(year=2014, month=5, day=6)),
                    (1, date(year=2014, month=3, day=5)),
                    (2, date(year=2013, month=11, day=7)),
                    (2, date(year=2013, month=8, day=8)))
        s = Stock.objects.create(name="ProSieben SAT.1 N", isin="DE000PSM7770")
        a = Appointment.objects.create(date=date(year=2014, month=8, day=7), stock=s, typ=2)
        save_appointments(s.isin, ParseResult(data=appo_res, errors=[]), mlogger)
        for i,a in enumerate(s.appointment_set.all().order_by("-date")):
            self.assertEqual([appo_indb[i][0], appo_indb[i][1]], [a.typ, a.date])

    def test_save_stock_quantity_in_db(self):
        mlogger = MemoryLogger("")
        s = Stock.objects.create(name="ProSieben SAT.1 N", isin="DE000PSM7770")
        save_stock_quantity(s.isin, ParseResult(data=[10], errors=[]), mlogger)
        d = datetime.today()
        d = date(year=d.year, month=d.month, day=d.day)
        for sq in s.stockquantity_set.all():
            self.assertEqual([d, 10], [sq.date, sq.quantity])

    def test_save_stockprices_in_db(self):
        mlogger = MemoryLogger("")
        stock_prices = (
                    ("31.10.14", 16.00, 16.00, 15.60, 16.00, 32730),
                    ("30.10.14", 16.00, 16.00, 15.87, 16.00, 5597),
                    ("29.10.14", 16.18, 16.18, 15.97 , 15.98, 23402),
                    ("28.10.14", 16.205, 16.205, 15.90 , 16.20, 15848),
                    ("27.10.14", 16.60, 16.60, 16.01 , 16.25, 45982),
                    ("24.10.14", 16.00, 16.70, 15.90 , 16.45, 22891),
                    ("23.10.14", 15.90, 16.265, 15.88 , 16.05, 22451),
                    ("22.10.14", 15.57, 15.935, 15.465 , 15.85, 69587),
                    ("21.10.14", 15.71, 15.71, 15.385 , 15.45, 19316),
                    ("20.10.14", 15.70, 15.70, 15.49 , 15.68, 11164),
                    ("17.10.14", 16.00, 16.195, 15.50 , 15.70, 17438),
                    ("16.10.14", 15.50, 16.225, 15.30 , 16.00, 68022),
                    ("15.10.14", 15.69, 15.69, 15.30 , 15.325, 14869),
                    ("14.10.14", 15.30, 15.755, 15.29 , 15.535, 37812),
                    ("13.10.14", 15.10, 15.62, 15.005 , 15.295, 16279),
                    ("10.10.14", 15.805, 15.815, 15.00 , 15.105, 108653),
                    ("09.10.14", 16.07, 16.295, 15.805 , 15.81, 41448),
                    ("08.10.14", 16.055, 16.20, 16.02 , 16.15, 42072),
                    ("07.10.14", 16.365, 16.50, 16.04 , 16.20, 20140),
                    ("06.10.14", 16.75, 16.845, 16.355 , 16.46, 35900),
                    ("02.10.14", 16.99, 16.99, 16.60 , 16.745, 7445),
                    ("01.10.14", 17.09, 17.20, 16.80 , 16.865, 26786),
                    )
        stock_prices_in_db = (
                    (make_date_object("31.10.14"), 1, 1, 1, 1, 1),
                    (make_date_object("30.10.14"), 16.00, 16.00, 15.87, 16.00, 5597),
                    (make_date_object("29.10.14"), 16.18, 16.18, 15.97 , 15.98, 23402),
                    (make_date_object("28.10.14"), 16.205, 16.205, 15.90 , 16.20, 15848),
                    (make_date_object("27.10.14"), 16.60, 16.60, 16.01 , 16.25, 45982),
                    (make_date_object("24.10.14"), 16.00, 16.70, 15.90 , 16.45, 22891),
                    (make_date_object("23.10.14"), 15.90, 16.265, 15.88 , 16.05, 22451),
                    (make_date_object("22.10.14"), 15.57, 15.935, 15.465 , 15.85, 69587),
                    (make_date_object("21.10.14"), 15.71, 15.71, 15.385 , 15.45, 19316),
                    (make_date_object("20.10.14"), 15.70, 15.70, 15.49 , 15.68, 11164),
                    (make_date_object("17.10.14"), 16.00, 16.195, 15.50 , 15.70, 17438),
                    (make_date_object("16.10.14"), 15.50, 16.225, 15.30 , 16.00, 68022),
                    (make_date_object("15.10.14"), 15.69, 15.69, 15.30 , 15.325, 14869),
                    (make_date_object("14.10.14"), 15.30, 15.755, 15.29 , 15.535, 37812),
                    (make_date_object("13.10.14"), 15.10, 15.62, 15.005 , 15.295, 16279),
                    (make_date_object("10.10.14"), 15.805, 15.815, 15.00 , 15.105, 108653),
                    (make_date_object("09.10.14"), 16.07, 16.295, 15.805 , 15.81, 41448),
                    (make_date_object("08.10.14"), 16.055, 16.20, 16.02 , 16.15, 42072),
                    (make_date_object("07.10.14"), 16.365, 16.50, 16.04 , 16.20, 20140),
                    (make_date_object("06.10.14"), 16.75, 16.845, 16.355 , 16.46, 35900),
                    (make_date_object("02.10.14"), 16.99, 16.99, 16.60 , 16.745, 7445),
                    (make_date_object("01.10.14"), 17.09, 17.20, 16.80 , 16.865, 26786),
                    )
        s = Stock.objects.create(name="ProSieben SAT.1 N", isin="DE000PSM7770")
        m = Market.objects.create(id=6, name="xetra")
        sp = StockPrice.objects.create(stock=s, market=m, date=make_date_object("31.10.14"), low=1, high=1, start=1, end=1, pieces=1)
        save_stockprices_month(s.isin, m.pk, ParseResult(data=stock_prices, errors=[]), mlogger)
        sl = s.stockprice_set.all().order_by("-date")
        self.assertEqual(len(sl), len(stock_prices_in_db))
        for i,p in enumerate(sl):
            self.assertEqual(stock_prices_in_db[i], (p.date, p.start, p.high, p.low, p.end, p.pieces))

    def test_save_indexprices_in_db(self):
        mlogger = MemoryLogger("")
        index_prices = (
            ("31.10.14", 9283.4, 9339.33, 9217, 9326.87),
            ("30.10.14", 9091.13, 9146.95, 8899.9, 9114.84),
            ("29.10.14", 9139, 9157.63, 9072.64, 9082.81),
            ("28.10.14", 8983.06, 9077.89, 8968.59, 9068.19),
            ("27.10.14", 9080.8, 9085.64, 8837.66, 8902.61),
            ("24.10.14", 9008.6, 9044.85, 8955.59, 8987.8),
            ("23.10.14", 8873.54, 9068.12, 8820.89, 9047.31),
            ("22.10.14", 8934.54, 8957.16, 8861.44, 8940.14),
            ("21.10.14", 8693.06, 8889.78, 8644.71, 8886.96),
            ("20.10.14", 8819.26, 8834.73, 8682.59, 8717.76),
            ("17.10.14", 8629.16, 8850.27, 8588.42, 8850.27),
            ("16.10.14", 8623.28, 8662.86, 8354.97, 8582.9),
            ("15.10.14", 8838.68, 8847.85, 8555.73, 8571.95),
            ("14.10.14", 8765.36, 8854.39, 8701.44, 8825.21),
            ("13.10.14", 8703.85, 8872.4, 8699.6, 8812.43),
            ("10.10.14", 8924.58, 8958.66, 8788.21, 8788.81),
            ("09.10.14", 9082.66, 9140.29, 8975.06, 9005.02),
            ("08.10.14", 9027.9, 9064.88, 8960.43, 8995.33),
            ("07.10.14", 9164.77, 9170.24, 9080.35, 9086.21),
            ("06.10.14", 9341.05, 9343.65, 9185.86, 9209.51),
            ("02.10.14", 9363.28, 9412.62, 9195.68, 9195.68),
            ("01.10.14", 9454.04, 9520.97, 9357.55, 9382.03),
                    )

        index_prices_in_db = (
            (make_date_object("31.10.14"), 1, 1, 1, 1),
            (make_date_object("30.10.14"), 9091.13, 9146.95, 8899.9, 9114.84),
            (make_date_object("29.10.14"), 9139, 9157.63, 9072.64, 9082.81),
            (make_date_object("28.10.14"), 8983.06, 9077.89, 8968.59, 9068.19),
            (make_date_object("27.10.14"), 9080.8, 9085.64, 8837.66, 8902.61),
            (make_date_object("24.10.14"), 9008.6, 9044.85, 8955.59, 8987.8),
            (make_date_object("23.10.14"), 8873.54, 9068.12, 8820.89, 9047.31),
            (make_date_object("22.10.14"), 8934.54, 8957.16, 8861.44, 8940.14),
            (make_date_object("21.10.14"), 8693.06, 8889.78, 8644.71, 8886.96),
            (make_date_object("20.10.14"), 8819.26, 8834.73, 8682.59, 8717.76),
            (make_date_object("17.10.14"), 8629.16, 8850.27, 8588.42, 8850.27),
            (make_date_object("16.10.14"), 8623.28, 8662.86, 8354.97, 8582.9),
            (make_date_object("15.10.14"), 8838.68, 8847.85, 8555.73, 8571.95),
            (make_date_object("14.10.14"), 8765.36, 8854.39, 8701.44, 8825.21),
            (make_date_object("13.10.14"), 8703.85, 8872.4, 8699.6, 8812.43),
            (make_date_object("10.10.14"), 8924.58, 8958.66, 8788.21, 8788.81),
            (make_date_object("09.10.14"), 9082.66, 9140.29, 8975.06, 9005.02),
            (make_date_object("08.10.14"), 9027.9, 9064.88, 8960.43, 8995.33),
            (make_date_object("07.10.14"), 9164.77, 9170.24, 9080.35, 9086.21),
            (make_date_object("06.10.14"), 9341.05, 9343.65, 9185.86, 9209.51),
            (make_date_object("02.10.14"), 9363.28, 9412.62, 9195.68, 9195.68),
            (make_date_object("01.10.14"), 9454.04, 9520.97, 9357.55, 9382.03),
                    )
        i = Index.objects.create(id=1, name="dax")
        sp = IndexPrice.objects.create(index=i,date=make_date_object("31.10.14"), low=1, high=1, start=1, end=1)
        save_indexprices_month(i.pk, ParseResult(data=index_prices, errors=[]), mlogger)
        il = i.indexprice_set.all().order_by("-date")
        self.assertEqual(len(il), len(index_prices_in_db))
        for i,p in enumerate(il):
            self.assertEqual(index_prices_in_db[i], (p.date, p.start, p.high, p.low, p.end))

