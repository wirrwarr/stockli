import os
import asyncio
from datetime import date, timedelta
from django.test import TestCase
from ..analysis import stock_analysis_large_cap, \
                        isabove, isbelow, \
                        calc_keyfigures_points, \
                        rel_ratio, get_ratio, \
                        calc_reaction_points, \
                        calc_price_points, \
                        calc_stockprice_reversal_points, \
                        handle_report_reaction, \
                        handle_keyfigures, \
                        handle_stockprices, \
                        handle_stockprice_reversal, \
                        AnalysisResult, \
                        loop_stock_analysis_large_cap, \
                        calc_keyfigures_points_2
from ..models import Stock, KeyFigures, Market, \
                    Appointment, StockPrice, Index, \
                    IndexPrice, KeyFigures2
from ..utils import DateMod, MemoryLogger, request_urls
from stockli import analysis

TEST_DIR = os.path.dirname(__file__)
ORG_GET = analysis.get

class AnalysisTest(TestCase):

    def test_handle_keyfigures_no_entries_in_db(self):
        stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
        def get_mock(x, y):
            if x == request_urls["keyfigures"] % stock.isin:
                html_file = os.path.join(TEST_DIR, "keyfigures.html")
                with open(html_file, encoding="iso-8859-1") as fh:
                    return fh.read()

        analysis.get = get_mock
        mlogger = MemoryLogger("")
        r = handle_keyfigures(stock, mlogger)
        analysis.get = ORG_GET
        self.assertEqual((-1, -1, 0, -1, 1, 1, 1, 1), r)

    def test_handle_keyfigures(self):
        tenv = create_good_test_env()
        mlogger = MemoryLogger("")
        r = handle_keyfigures(tenv[0], mlogger)
        self.assertEqual((1,1,1,1,1,1,1,1), r)

    def test_handle_report_reaction(self):
        mlogger = MemoryLogger("")
        tenv = create_good_test_env()
        r = handle_report_reaction(tenv[0], tenv[1], tenv[2], mlogger)
        self.assertEqual(1, r)

    def test_handle_report_reaction_no_entries_in_db(self):
        stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
        index = Index.objects.create(id=1, name="DAX")
        market = Market.objects.create(id=6, name="xetra")
        cur_date = date(year=2014, month=10, day=22)
        mlogger = MemoryLogger("")
        def get_mock(x, y):
            if x == request_urls["appointments"] % "adidas":
                html_file = os.path.join(TEST_DIR, "appointments4.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-10-08"):
                html_file = os.path.join(TEST_DIR, "stockprices1.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-10-08"):
                html_file = os.path.join(TEST_DIR, "indexprices1.html")

            with open(html_file, encoding="iso-8859-1") as fh:
                return fh.read()

        analysis.get = get_mock
        r = handle_report_reaction(stock, index, cur_date, mlogger)
        analysis.get = ORG_GET
        self.assertEqual(0, r)

    def test_handle_stockprices_no_entries_in_db(self):
        stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
        index = Index.objects.create(id=1, name="DAX")
        market = Market.objects.create(id=6, name="xetra")
        cur_date = date(year=2014, month=10, day=22)
        mlogger = MemoryLogger("")
        def get_mock(x, y):
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-10-21"):
                html_file = os.path.join(TEST_DIR, "stockprices_201410.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-04-21"):
                html_file = os.path.join(TEST_DIR, "stockprices_201404.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2013-10-23"):
                html_file = os.path.join(TEST_DIR, "stockprices_201310.html")

            with open(html_file, encoding="iso-8859-1") as fh:
                return fh.read()

        analysis.get = get_mock
        r = handle_stockprices(stock, cur_date, mlogger)
        analysis.get = ORG_GET
        self.assertEqual((1, 1, 0), r)

    def test_handle_stockprices(self):
        tenv = create_good_test_env()
        mlogger = MemoryLogger("")
        r = handle_stockprices(tenv[0], tenv[2], mlogger)
        self.assertEqual((1,1,0), r)

    def test_handle_stockprice_reversal_no_entries_in_db(self):
        stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
        index = Index.objects.create(id=1, name="DAX")
        market = Market.objects.create(id=6, name="xetra")
        cur_date = date(year=2014, month=10, day=22)
        mlogger = MemoryLogger("")
        def get_mock(x, y):
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-09-30"):
                html_file = os.path.join(TEST_DIR, "stockprices_201409.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-08-29"):
                html_file = os.path.join(TEST_DIR, "stockprices_201408.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-07-31"):
                html_file = os.path.join(TEST_DIR, "stockprices_201407.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-06-30"):
                html_file = os.path.join(TEST_DIR, "stockprices_201406.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-09-30"):
                html_file = os.path.join(TEST_DIR, "indexprices_201409.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-08-29"):
                html_file = os.path.join(TEST_DIR, "indexprices_201408.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-07-31"):
                html_file = os.path.join(TEST_DIR, "indexprices_201407.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-06-30"):
                html_file = os.path.join(TEST_DIR, "indexprices_201406.html")

            with open(html_file, encoding="iso-8859-1") as fh:
                return fh.read()

        analysis.get = get_mock
        r = handle_stockprice_reversal(stock, index, cur_date, mlogger)
        analysis.get = ORG_GET
        self.assertEqual(0, r)

    def test_handle_stockprice_reversal(self):
        tenv = create_good_test_env()
        mlogger =  MemoryLogger("")
        r = handle_stockprice_reversal(tenv[0], tenv[1], tenv[2], mlogger)
        self.assertEqual(-1, r)

    def test_stock_analysis_large_capbasic(self):
        tenv = create_good_test_env()
        dqueue, logger = loop_stock_analysis_large_cap([tenv[0]], tenv[1], tenv[2])
        expected_result = AnalysisResult(tenv[0], date, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, -1)
        self.assertEqual(str(expected_result), str(dqueue.objects()[0]))
        self.assertEqual(expected_result.points(), dqueue.objects()[0].points())

    def test_stock_analysis_large_cap_no_entries_in_db(self):
        stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
        index = Index.objects.create(id=1, name="DAX")
        market = Market.objects.create(id=6, name="xetra")
        cur_date = date(year=2014, month=10, day=22)
        mlogger = MemoryLogger("")

        def get_mock(x, y):
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-09-30"):
                html_file = os.path.join(TEST_DIR, "stockprices_201409.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-08-29"):
                html_file = os.path.join(TEST_DIR, "stockprices_201408.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-07-31"):
                html_file = os.path.join(TEST_DIR, "stockprices_201407.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-06-30"):
                html_file = os.path.join(TEST_DIR, "stockprices_201406.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-09-30"):
                html_file = os.path.join(TEST_DIR, "indexprices_201409.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-08-29"):
                html_file = os.path.join(TEST_DIR, "indexprices_201408.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-07-31"):
                html_file = os.path.join(TEST_DIR, "indexprices_201407.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-06-30"):
                html_file = os.path.join(TEST_DIR, "indexprices_201406.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-10-21"):
                html_file = os.path.join(TEST_DIR, "stockprices_201410.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-04-21"):
                html_file = os.path.join(TEST_DIR, "stockprices_201404.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2013-10-23"):
                html_file = os.path.join(TEST_DIR, "stockprices_201310.html")
            if x == request_urls["appointments"] % "adidas":
                html_file = os.path.join(TEST_DIR, "appointments4.html")
            if x == request_urls["stockprices_month"] % ("DE000A1EWWW0", "2014-10-08"):
                html_file = os.path.join(TEST_DIR, "stockprices1.html")
            if x == request_urls["indexprices_month"] % ("DAX", "2014-10-08"):
                html_file = os.path.join(TEST_DIR, "indexprices1.html")
            if x == request_urls["keyfigures"] % stock.isin:
                html_file = os.path.join(TEST_DIR, "keyfigures.html")

            with open(html_file, encoding="iso-8859-1") as fh:
                return fh.read()
        analysis.get = get_mock
        dqueue, logger = loop_stock_analysis_large_cap([stock], index, cur_date)
        expected_result = AnalysisResult(stock=stock, date=date, \
                                         roi=-1, return_on_sales=-1, equity_ratio=0, \
                                         liquidity1=-1, per=1, per_5y=1, pcr=1, pcr_5y=1, \
                                         stockprice_6m=1, stockprice_1y=1, pricemomentum=0,\
                                         reaction_quartalfigures=0, stockprice_reversal=0)
        analysis.get = ORG_GET
        self.assertEqual(str(expected_result), str(dqueue.objects()[0]))
        self.assertEqual(expected_result.points(), dqueue.objects()[0].points())

class PointCalcTestCase(TestCase):
    def test_calc_stockprice_reversal(self):
        self.assertEqual(-1, calc_stockprice_reversal_points([1,2,3,4],[1,1,1,1]))
        self.assertEqual(0, calc_stockprice_reversal_points([1,2,3,2],[1,2,3,4]))
        self.assertEqual(1, calc_stockprice_reversal_points([0.443,0.442,0.2,0.21],[5.5,7.5,8.5,9]))

    def test_calc_price_points(self):
        self.assertEqual(0, calc_price_points(1,1))
        self.assertEqual(0, calc_price_points(10, 10.5))
        self.assertEqual(1, calc_price_points(10, 18))
        self.assertEqual(-1, calc_price_points(10, 8))

    def test_rel_ratio(self):
        self.assertEqual(11.0, rel_ratio(100, 111))
        self.assertEqual(-10.0, rel_ratio(100, 90))

    def test_get_ratio(self):
        self.assertEqual(250.0, get_ratio(40, 100))
        self.assertEqual(50.0, get_ratio(10, 5))

    def test_isbigger(self):
        self.assertEqual(0, isabove(5, 5, 10))
        self.assertEqual(1, isabove(11, 5, 10))
        self.assertEqual(-1, isabove(4, 5, 10))
        self.assertEqual(0, isabove(-7, -10, -5))
        self.assertEqual(1, isabove(-1, -10, -5))
        self.assertEqual(-1, isabove(-11, -10, -5))

    def test_isbelow(self):
        self.assertEqual(0, isbelow(5, 5, 10))
        self.assertEqual(-1, isbelow(11, 5, 10))
        self.assertEqual(1, isbelow(-1, 5, 10))
        self.assertEqual(0, isbelow(-10, -10, -5))
        self.assertEqual(1, isbelow(-12, -10, -5))
        self.assertEqual(-1, isbelow(12, -10, -5))

    def test_calc_reaction_points(self):
        self.assertEqual(0, calc_reaction_points(1,2,1,2))
        self.assertEqual(1, calc_reaction_points(1,4,1,2))
        self.assertEqual(-1, calc_reaction_points(4,1,1,2))
        self.assertEqual(1, calc_reaction_points(4,1,6,1))
        self.assertEqual(-1, calc_reaction_points(1,3,1,6))

    def test_calc_keyfigures_points(self):
        tenv = create_good_test_env()
        c = calc_keyfigures_points(tenv[4])
        self.assertEqual((1,1,1,1,1,1,1,1), c)

    def test_calc_keyfigures_points_2(self):
        tenv = create_good_test_env()
        c = calc_keyfigures_points_2(tenv[5])
        self.assertEqual((1,1,1,1,1), c)

def create_good_test_env():
    cur_date = date(day=7, month=11, year=2014)
    cur_date_mod = DateMod(date_obj=cur_date)
    stock = Stock.objects.create(isin="DE000A1EWWW0", name="adidas")
    index = Index.objects.create(id=1, name="DAX")
    market = Market.objects.create(id=6, name="xetra")
    keyfigures = []
    keyfigures2 = []
    for i in range(cur_date.year-5, cur_date.year):
        keyfigures.append(KeyFigures.objects.create(stock=stock,
                            year=i,
                            per=10,
                            pcr=10,
                            psr=-1,
                            pbr=-1,
                            roi=21,
                            return_on_sales=13,
                            equity_ratio=26,
                            debt_ratio=-1,
                            cashflow=-1,
                            liquidity=76))
        keyfigures2.append(KeyFigures2.objects.create(stock=stock,
                            year=i,
                            per=10,
                            roe=21,
                            ebit_marge=13,
                            equity_ratio=26))

    # Reaktion auf Quartalszahlen
    a1 = Appointment.objects.create(stock=stock, date=date(day=6, month=11, year=2014), typ=1)
    sp1 = StockPrice.objects.create(stock=stock, market=market, date=a1.date, start=1, end=5, low=2, high=3, pieces=1)
    sp2 = StockPrice.objects.create(stock=stock, market=market, date=a1.date-timedelta(days=1), start=1, end=5, low=2, high=3, pieces=1)
    sp3 = StockPrice.objects.create(stock=stock, market=market, date=a1.date+timedelta(days=1), start=1, end=10, low=2, high=4, pieces=1)
    
    ip1 = IndexPrice.objects.create(index=index, date=a1.date, start=1, end=4, low=2, high=3)
    ip2 = IndexPrice.objects.create(index=index, date=a1.date-timedelta(days=1), start=1, end=4, low=2, high=3)
    ip3 = IndexPrice.objects.create(index=index, date=a1.date+timedelta(days=1), start=1, end=7, low=2, high=4)

    # Dreimontsreversal
    d1 = date(year=2014, month=10, day=31)
    d2 = date(year=2014, month=9, day=30)
    d3 = date(year=2014, month=8, day=29)
    d4 = date(year=2014, month=7, day=31)

    sp4 = StockPrice.objects.create(stock=stock, market=market, date=d1, start=1, end=4, low=1, high=1)
    sp5 = StockPrice.objects.create(stock=stock, market=market, date=d2, start=2, end=5, low=1, high=1)
    sp6 = StockPrice.objects.create(stock=stock, market=market, date=d3, start=3, end=7, low=1, high=1)
    sp6 = StockPrice.objects.create(stock=stock, market=market, date=d4, start=3, end=19, low=1, high=1)
      
    ip4 = IndexPrice.objects.create(index=index, date=d1, start=1, end=4, low=1, high=1)
    ip5 = IndexPrice.objects.create(index=index, date=d2, start=2, end=4, low=1, high=1)
    ip6 = IndexPrice.objects.create(index=index, date=d3, start=3, end=5, low=1, high=1)
    ip6 = IndexPrice.objects.create(index=index, date=d4, start=3, end=5, low=1, high=1)

    # Kurs veränderung
    d_6m = date(year=2014, month=5, day=6)
    d_1y = date(year=2013, month=11, day=8)
    sp7 = StockPrice.objects.create(stock=stock, market=market, date=d_6m, start=-1, end=8, low=1, high=1)
    sp8 = StockPrice.objects.create(stock=stock, market=market, date=d_1y, start=-1, end=9, low=1, high=1)

    return (stock, index, cur_date, a1, keyfigures, keyfigures2, \
            [sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8], \
            [ip1, ip2, ip3, ip4, ip5, ip6])
