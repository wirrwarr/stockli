import os
from django.test import TestCase
from ..parser.finanzen import parse_index_page, parse_dividend_page, \
                            parse_appointments_page
from ..parser.ariva import parse_stockprices_month_page, parse_indexprices_month_page
from ..parser.wallstreetonline import parse_keyfigures_page
from ..parser.onvista import parse_keyfigures_page_2, parse_stock_quantity_page

TEST_DIR = os.path.dirname(__file__)

class IndexStocksTestCase(TestCase):
    def test_parse_finanzen_html(self):
        index_stocks_org = [("US90130A1016", "21st Century Fox A"),
                ("US88579Y1010", "3M"),
                ("US0028241000", "Abbott Laboratories"),
                ("US00287Y1091", "AbbVie"),
                ("IE00B4BNMY34", "Accenture"),
                ("CH0044328745", "ACE 2"),
                ("IE00BD1NQJ95", "Actavis 2")]

        html_file = os.path.join(TEST_DIR, "index_stocks_p1.html") 
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        self.assertEqual(tuple(index_stocks_org), parse_index_page(html_code).data)


class DividendTestCase(TestCase):
    def test_parse_finanzen_html(self):
        dividend_result = (
                    (2013,0.35),
                    (2012,0.25),
                    (2011,0.13),
                    (2010,0.10),
                    (2009,0.30),
                    (2008,8.1),
                    (2007,0.0),
                    (2006,0.0))
 
        html_file = os.path.join(TEST_DIR, "dividend.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_dividend_page(html_code) 
        self.assertEqual(dividend_result, r.data)
        self.assertEqual(1, len(r.errors))

class KeyFiguresTestCase(TestCase):
    def test_parse_wallstreetonline_html(self):
        keyfigures_result = {2009: {"per": 11.5,
                                    "pcr": 1.16,
                                    "psr": 0.62,
                                    "pbr": 2.95,
                                    "return_on_sales": 5.23,
                                    "roi": 2.34,
                                    "equity_ratio": 9.41,
                                    "debt_ratio": 90.59,
                                    "liquidity": 52.13,
                                    },
                             2010: {"per": 15.20,
                                    "pcr": 3.06,
                                    "psr": 1.64,
                                    "pbr": 4.80,
                                    "return_on_sales": 10.42,
                                    "roi": 4.95,
                                    "equity_ratio": 16.24,
                                    "debt_ratio": 83.76,
                                    "liquidity": 62.87,
                                    },
                            }
        html_file = os.path.join(TEST_DIR, "keyfigures.html")
        with open(html_file, encoding="utf-8") as fh:
            html_code = fh.read()
        r = parse_keyfigures_page(html_code) 

        for y in range(2009, 2013):
            self.assertTrue(y in r.data)

        for y,f in keyfigures_result.items():
            for k,v in f.items():
                self.assertEqual(v, r.data[y][k])

    def test_parse_wallstreetonline_html2(self):
        keyfigures_result = {2012: {"per": -1,
                                    "pcr": -1,
                                    "psr": -1,
                                    "pbr": -1,
                                    "return_on_sales": 1.87,
                                    "roi": 1.56,
                                    "equity_ratio": 10.82,
                                    "debt_ratio": 89.18,
                                    "liquidity": 50.49,
                                    },
                             2013: {"per": -1,
                                    "pcr": -1,
                                    "psr": -1,
                                    "pbr": -1,
                                    "return_on_sales": -3.46,
                                    "roi": -2.7,
                                    "equity_ratio": 14.02,
                                    "debt_ratio": 85.98,
                                    "liquidity": 24.80,
                                    },
                            }
        html_file = os.path.join(TEST_DIR, "keyfigures2.html")
        with open(html_file, encoding="utf-8") as fh:
            html_code = fh.read()
        r = parse_keyfigures_page(html_code) 

        for y in range(2012, 2013):
            self.assertTrue(y in r.data)

        for y,f in keyfigures_result.items():
            for k,v in f.items():
                self.assertEqual(v, r.data[y][k])

class AppointmentsTestCase(TestCase):
    def test_parse_appointments_adidas_page(self):
        appo_res = ((2, "07.08.14"),
                    (2, "06.05.14"),
                    (1, "05.03.14"),
                    (2, "07.11.13"),
                    (2, "08.08.13"))
        html_file = os.path.join(TEST_DIR, "appointments2.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_appointments_page(html_code) 
        self.assertEqual(appo_res, r.data)

    def test_parse_appointments_page2(self):
        appo_res = ()
        html_file = os.path.join(TEST_DIR, "appointments3.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_appointments_page(html_code) 
        self.assertEqual(appo_res, r.data)

class StockQuantityTestCase(TestCase):
    def test_parse_stock_quantity_page(self):
        html_file = os.path.join(TEST_DIR, "stock_quantity_p1.html")
        with open(html_file, encoding="utf-8") as fh:
            html_code = fh.read()
        r = parse_stock_quantity_page(html_code) 
        self.assertEqual(1069837447, r.data[0])

class StockPricesTestCase(TestCase):
    def test_parse_stock_prices_page(self):
        stock_prices = (
                    ("07.11.14", 33.47, 33.635, 32.60 , 32.83, 225448),
                    ("06.11.14", 33.70, 34.025, 33.00 , 33.315, 202831),
                    ("05.11.14", 33.71, 34.165, 33.61 , 33.725, 129915),
                    ("04.11.14", 33.995, 34.465, 33.40 , 33.56, 325481),
                    ("03.11.14", 34.20, 34.52, 33.63 , 33.87, 270763),
                    )


        html_file = os.path.join(TEST_DIR, "stockprices2.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_stockprices_month_page(html_code)
        self.assertEqual(0, len(r.errors))
        for i,p in enumerate(stock_prices):
            self.assertEqual(p, r.data[i])

    def test_parse_stock_prices_page2(self):
        stock_prices = (
                    ("31.10.14", 16.00, 16.00, 15.60, 16.00, 32730),
                    ("30.10.14", 16.00, 16.00, 15.87, 16.00, 5597),
                    ("29.10.14", 16.18, 16.18, 15.97 , 15.98, 23402),
                    ("28.10.14", 16.205, 16.205, 15.90 , 16.20, 15848),
                    ("27.10.14", 16.60, 16.60, 16.01 , 16.25, 45982),
                    ("24.10.14", 16.00, 16.70, 15.90 , 16.45, 22891),
                    ("23.10.14", 15.90, 16.265, 15.88 , 16.05, 22451),
                    ("22.10.14", 15.57, 15.935, 15.465 , 15.85, 69587),
                    ("21.10.14", 15.71, 15.71, 15.385 , 15.45, 19316),
                    ("20.10.14", 15.70, 15.70, 15.49 , 15.68, 11164),
                    ("17.10.14", 16.00, 16.195, 15.50 , 15.70, 17438),
                    ("16.10.14", 15.50, 16.225, 15.30 , 16.00, 68022),
                    ("15.10.14", 15.69, 15.69, 15.30 , 15.325, 14869),
                    ("14.10.14", 15.30, 15.755, 15.29 , 15.535, 37812),
                    ("13.10.14", 15.10, 15.62, 15.005 , 15.295, 16279),
                    ("10.10.14", 15.805, 15.815, 15.00 , 15.105, 108653),
                    ("09.10.14", 16.07, 16.295, 15.805 , 15.81, 41448),
                    ("08.10.14", 16.055, 16.20, 16.02 , 16.15, 42072),
                    ("07.10.14", 16.365, 16.50, 16.04 , 16.20, 20140),
                    ("06.10.14", 16.75, 16.845, 16.355 , 16.46, 35900),
                    ("02.10.14", 16.99, 16.99, 16.60 , 16.745, 7445),
                    ("01.10.14", 17.09, 17.20, 16.80 , 16.865, 26786),
                    )


        html_file = os.path.join(TEST_DIR, "stockprices1.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_stockprices_month_page(html_code)
        self.assertEqual(0, len(r.errors))
        for i,p in enumerate(stock_prices):
            self.assertEqual(p, r.data[i])

class IndexPricesTestCase(TestCase):
    def test_parse_index_prices_page(self):
        index_prices = (
            ("31.10.14", 9283.4, 9339.33, 9217, 9326.87),
            ("30.10.14", 9091.13, 9146.95, 8899.9, 9114.84),
            ("29.10.14", 9139, 9157.63, 9072.64, 9082.81),
            ("28.10.14", 8983.06, 9077.89, 8968.59, 9068.19),
            ("27.10.14", 9080.8, 9085.64, 8837.66, 8902.61),
            ("24.10.14", 9008.6, 9044.85, 8955.59, 8987.8),
            ("23.10.14", 8873.54, 9068.12, 8820.89, 9047.31),
            ("22.10.14", 8934.54, 8957.16, 8861.44, 8940.14),
            ("21.10.14", 8693.06, 8889.78, 8644.71, 8886.96),
            ("20.10.14", 8819.26, 8834.73, 8682.59, 8717.76),
            ("17.10.14", 8629.16, 8850.27, 8588.42, 8850.27),
            ("16.10.14", 8623.28, 8662.86, 8354.97, 8582.9),
            ("15.10.14", 8838.68, 8847.85, 8555.73, 8571.95),
            ("14.10.14", 8765.36, 8854.39, 8701.44, 8825.21),
            ("13.10.14", 8703.85, 8872.4, 8699.6, 8812.43),
            ("10.10.14", 8924.58, 8958.66, 8788.21, 8788.81),
            ("09.10.14", 9082.66, 9140.29, 8975.06, 9005.02),
            ("08.10.14", 9027.9, 9064.88, 8960.43, 8995.33),
            ("07.10.14", 9164.77, 9170.24, 9080.35, 9086.21),
            ("06.10.14", 9341.05, 9343.65, 9185.86, 9209.51),
            ("02.10.14", 9363.28, 9412.62, 9195.68, 9195.68),
            ("01.10.14", 9454.04, 9520.97, 9357.55, 9382.03),
                    )
        html_file = os.path.join(TEST_DIR, "indexprices1.html")
        with open(html_file, encoding="iso-8859-1") as fh:
            html_code = fh.read()
        r = parse_indexprices_month_page(html_code)
        self.assertEqual(0, len(r.errors))
        for i,p in enumerate(index_prices):
            self.assertEqual(p, r.data[i])

class KeyFigures2TestCase(TestCase):
    def test_parse_onvista_html(self):
        keyfigures_result = {"2011": {"per": 9.73,
                                    "ebit_marge": 34.54,
                                    "roe": 44.22,
                                    "equity_ratio": 28.63,
                                    },
                             "2010": {"per": 13.55,
                                    "ebit_marge": 21.62,
                                    "roe": 30.48,
                                    "equity_ratio": 16.24,
                                    },
                            }
        html_file = os.path.join(TEST_DIR, "keyfigures3.html")
        with open(html_file, encoding="utf-8") as fh:
            html_code = fh.read()
        r = parse_keyfigures_page_2(html_code) 

        for y in range(2010, 2011):
            y = str(y)
            self.assertTrue(y in r.data)

        for y,f in keyfigures_result.items():
            for k,v in f.items():
                self.assertEqual(v, r.data[y][k])

    def test_parse_onvista_html2(self):
        keyfigures_result = {"2011": {"per": 9.28,
                                    "ebit_marge": 11.03,
                                    "roe": 19.10,
                                    "equity_ratio": 30.84,
                                    },
                             "2010": {"per": 15.50,
                                    "ebit_marge": 7.13,
                                    "roe": 13.40,
                                    "equity_ratio": 28.29,
                                    },
                            }
        html_file = os.path.join(TEST_DIR, "keyfigures4.html")
        with open(html_file, encoding="utf-8") as fh:
            html_code = fh.read()
        r = parse_keyfigures_page_2(html_code) 

        for y in range(2010, 2011):
            y = str(y)
            self.assertTrue(y in r.data)

        for y,f in keyfigures_result.items():
            for k,v in f.items():
                self.assertEqual(v, r.data[y][k])
