from django.conf.urls import patterns, include, url
from .views import view_stocks_from_index, view_dividends, \
                   view_keyfigures, view_appointments, \
                   view_stockprices_month, view_stock_analysis, \
                   view_indexprices_month, view_stock_analysis_2, \
                   view_stock_quantity

urlpatterns = patterns('',
    url(r'req_index_stocks/$', view_stocks_from_index, name="stockli_view_index_stocks"),
    url(r'req_dividend/$', view_dividends, name="stockli_view_dividends"),
    url(r'req_stock_quantity/$', view_stock_quantity, name="stockli_view_stock_quantity"),
    url(r'req_keyfigures/$', view_keyfigures, name="stockli_view_keyfigures"),
    url(r'req_appointments/$', view_appointments, name="stockli_view_appointments"),
    url(r'req_stockprices_month/$', view_stockprices_month, name="stockli_view_stockprices_month"),
    url(r'req_indexprices_month/$', view_indexprices_month, name="stockli_view_indexprices_month"),
    url(r'stock_analysis/$', view_stock_analysis, name="stockli_view_stock_analysis"),
    url(r'stock_analysis2/$', view_stock_analysis_2, name="stockli_view_stock_analysis_2"),
)
