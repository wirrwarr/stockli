import asyncio
import aiohttp
from datetime import date, timedelta
from .models import Appointment, StockPrice, IndexPrice, Index
from .utils import find_pre_post_days, \
                    request_urls, make_stock_url_ready, \
                    DataQueue, DateMod, find_last_weekday_month, \
                    MemoryLogger, find_last_day_month, \
                    query_indices_stocks
from .handlers import handler_appointments_page, \
                      handler_stockprices_month_page, \
                      handler_indexprices_month_page, \
                      handler_keyfigures_page, \
                      handler_keyfigures_page_2, \
                      handler_stock_quantity_page

@asyncio.coroutine
def get(url, logger):
    header = {"User-agent": 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36',
              "Content-Type": 'text/html'}
    response = yield from aiohttp.request('GET', url, headers=header)
    #print(response)
    if response.status == 200:
        logger.info("%s OK!" % (url,))
    else:
        logger.error("%s FAILED!" % (url,))
    return (yield from response.read_and_close())

def loop_stock_analysis_large_cap(stocks, ref_index, dat):
    dqueue = DataQueue("analysis_results")
    mlogger = MemoryLogger("analysis_logs")
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = [asyncio.async( \
                stock_analysis_large_cap(stock, ref_index, \
                                         dat, dqueue, mlogger) \
                ) 
                for stock in stocks]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
    return (dqueue, mlogger)

def loop_stock_analysis_large_cap_2(stocks, ref_index, dat):
    dqueue = DataQueue("analysis_results")
    mlogger = MemoryLogger("analysis_logs")
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    tasks = [asyncio.async( \
                stock_analysis_large_cap_2(stock, ref_index, \
                                         dat, dqueue, mlogger) \
                ) 
                for stock in stocks]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
    return (dqueue, mlogger)

@asyncio.coroutine
def stock_analysis_large_cap_2(stock, ref_index, cur_date, dqueue, logger):
    kf_r = yield from handle_keyfigures_2(stock, cur_date, logger)
    rr_r = yield from handle_report_reaction(stock, ref_index, cur_date, logger)
    sp_r = yield from handle_stockprices(stock, cur_date, logger)
    spr_r = yield from handle_stockprice_reversal(stock, ref_index, cur_date, logger)
    company_size = yield from handle_company_size(stock, cur_date, logger)
    
    if kf_r is None:
        kf_r = [None, None, None, None, None]
    if sp_r is None:
        sp_r = [None, None, None]
    
    rtmp = AnalysisResult2(stock=stock, date=cur_date, \
                          roe=kf_r[0], ebit_marge=kf_r[1], \
                          equity_ratio=kf_r[2], \
                          per=kf_r[3], per_5y=kf_r[4], \
                          reaction_quartalfigures=rr_r, \
                          stockprice_6m=sp_r[0], stockprice_1y=sp_r[1], \
                          pricemomentum=sp_r[2], \
                          stockprice_reversal=spr_r)

    analysis_return = AnalysisReturn2(stock=stock, analysis_result=rtmp, meta=AnalysisMeta(company_size=company_size))

    dqueue.add(analysis_return)

@asyncio.coroutine
def stock_analysis_large_cap(stock, ref_index, cur_date, dqueue, logger):
    kf_r = yield from handle_keyfigures(stock, cur_date, logger)
    rr_r = yield from handle_report_reaction(stock, ref_index, cur_date, logger)
    sp_r = yield from handle_stockprices(stock, cur_date, logger)
    spr_r = yield from handle_stockprice_reversal(stock, ref_index, cur_date, logger)
    
    if kf_r is None:
        kf_r = [None, None, None, None, None, None, None, None]
    if sp_r is None:
        sp_r = [None, None, None]
    
    rtmp = AnalysisResult(stock=stock, date=cur_date, \
                          roi=kf_r[0], return_on_sales=kf_r[1], \
                          equity_ratio=kf_r[2], liquidity1=kf_r[3], \
                          per=kf_r[4], pcr=kf_r[5], per_5y=kf_r[6], \
                          pcr_5y=kf_r[7], reaction_quartalfigures=rr_r, \
                          stockprice_6m=sp_r[0], stockprice_1y=sp_r[1], \
                          pricemomentum=sp_r[2], \
                          stockprice_reversal=spr_r)

    dqueue.add(rtmp)

@asyncio.coroutine
def try_get_indexprices(index, pdate, dqueue, logger):
    h = handler_indexprices_month_page(index.pk, dqueue, logger)
    html = yield from get(request_urls["indexprices_month"] % (index.name, pdate), logger)
    h(html)

@asyncio.coroutine
def try_get_stockprices(stock, pdate, dqueue, logger):
    h = handler_stockprices_month_page(stock.isin, 6, dqueue, logger)
    html = yield from get(request_urls["stockprices_month"] % (stock.isin, pdate), logger)
    h(html)

@asyncio.coroutine
def try_get_stockprices_pre_post_days(stock, pre_d, post_d, dqueue, logger):
    yield from try_get_stockprices(stock, get_last_day_month(post_d), dqueue, logger)
    if pre_d.month != post_d.month:
        yield from try_get_stockprices(stock, get_last_day_month(pre_d), dqueue, logger)

def handle_stockprice_reversal(stock, index, cur_date, logger):
    dmod = DateMod(date_obj=cur_date)
    five_days = timedelta(days=5)

    dates = []
    for i in range(1, 5):
        dtmp = dmod.mod_month(-i)
        dates.append(find_last_weekday_month(dtmp.month, dtmp.year))

    sps = []
    ips = []
    for i in range(4):
        sps.append(stock.stockprice_set \
                             .filter(date__range=(dates[i]-five_days, \
                                                  dates[i])))
        ips.append(index.indexprice_set 
                             .filter(date__range=(dates[i]-five_days, \
                                                  dates[i])))
    for i in range(4):
        if len(sps[i]) == 0:
            dqueue = DataQueue("")
            yield from try_get_stockprices(stock, dates[i], dqueue, logger)
            sps[i] = sps[i].all()
            if len(sps[i]) == 0:
                return None
        if len(ips[i]) == 0:
            dqueue = DataQueue("")
            yield from try_get_indexprices(index, dates[i], dqueue, logger)
            ips[i] = ips[i].all()
            if len(ips[i]) == 0: 
                return None

    sps_end = [sp[0].end for sp in sps]
    ips_end = [ip[0].end for ip in ips]

    return calc_stockprice_reversal_points(sps_end, ips_end)

def handle_stockprices(stock, cur_date, logger):
    dm = DateMod(date_obj=cur_date)
    five_days = timedelta(days=5)
    yday = find_pre_post_days(cur_date)[0]
    d_6m = find_pre_post_days(dm.mod_month(-6))[0]
    d_1y = find_pre_post_days(dm.mod_year(-1))[1]

    sp_cur = stock.stockprice_set.filter(date__range=(yday-five_days, yday))
    if len(sp_cur) == 0:
        dqueue = DataQueue("")
        dtmp = find_last_day_month(yday.month, yday.year)
        yield from try_get_stockprices(stock, dtmp, dqueue, logger)
        sp_cur = sp_cur.all()
        if len(sp_cur) == 0:
            return None

    sp_6m = stock.stockprice_set.filter(date__range=(d_6m-five_days , d_6m))
    if len(sp_6m) == 0:
        dqueue = DataQueue("")
        dtmp = find_last_day_month(d_6m.month, d_6m.year)
        yield from try_get_stockprices(stock, dtmp, dqueue, logger)
        sp_6m = sp_6m.all()

    sp_1y = stock.stockprice_set.filter(date__range=(d_1y-five_days, d_1y))
    if len(sp_1y) == 0:
        dqueue = DataQueue("")
        dtmp = find_last_day_month(d_1y.month, d_1y.year)
        yield from try_get_stockprices(stock, dtmp, dqueue, logger)
        sp_1y = sp_1y.all()

    points_6m = None
    if len(sp_6m) != 0:
        points_6m = calc_price_points(sp_cur[0].end, sp_6m[0].end)
    points_1y = None
    if len(sp_1y) != 0:
        points_1y = calc_price_points(sp_cur[0].end, sp_1y[0].end)

    points_mom = None
    if len(sp_6m) != 0 and len(sp_1y) != 0:
        if points_1y <= 0 and points_6m == 1:
            points_mom = 1
        elif points_1y > 0 and points_6m == -1:
            points_mom = -1
        else:
            points_mom = 0

    return (points_6m, points_1y, points_mom)

def handle_keyfigures(stock, cur_date, logger):
    kf = stock.keyfigures_set.filter(year__lte=cur_date.year).order_by("-year")[0:5]
    if len(kf) < 5:
        dqueue = DataQueue("")
        h = handler_keyfigures_page(stock.isin, dqueue, logger)
        html = yield from get(request_urls["keyfigures"] % stock.isin, logger)
        h(html)
        kf = stock.keyfigures_set.order_by("-year")[0:5]
        if len(kf) < 3:
            return None

    return calc_keyfigures_points(kf)

def handle_company_size(stock, cur_date, logger):
    dm = DateMod(date_obj=cur_date)
    five_days = timedelta(days=5)
    d_1y = find_pre_post_days(dm.mod_year(-1))[1]
    yday = find_pre_post_days(cur_date)[0]
    sq = stock.stockquantity_set.filter(date__gte=d_1y).order_by("-date")
    if len(sq) == 0:
        dqueue = DataQueue("")
        h = handler_stock_quantity_page(stock.isin, dqueue, logger)
        url = request_urls["stock_quantity"] % (stock.name, stock.isin)
        html = yield from get(url, logger)
        h(html)
        sq = sq.all()
        if len(sq) == 0:
            return None

    sp_cur = stock.stockprice_set.filter(date__range=(yday-five_days, yday))
    if len(sp_cur) == 0:
        dqueue = DataQueue("")
        dtmp = find_last_day_month(yday.month, yday.year)
        yield from try_get_stockprices(stock, dtmp, dqueue, logger)
        sp_cur = sp_cur.all()
        if len(sp_cur) == 0:
            return None
    
    mk = sp_cur[0].end * sq[0].quantity
    dax = Index.objects.filter(name="DAX")[0]
    dow = Index.objects.filter(name="Dow Jones")[0]
    r = query_indices_stocks([dax, dow])

    if mk >= 5000000000 or stock in r:
        # Big company
        return 0
    else:
        # no big company :)
        return 1

def handle_keyfigures_2(stock, cur_date, logger):
    kf = stock.keyfigures2_set.filter(year__lte=cur_date.year).order_by("-year")[0:5]
    if len(kf) < 4:
        dqueue = DataQueue("")
        h = handler_keyfigures_page_2(stock.isin, dqueue, logger)
        html = yield from get(request_urls["keyfigures_2"] % (stock.name, stock.isin), logger)
        h(html)
        kf = stock.keyfigures2_set.order_by("-year")[0:5]
        if len(kf) < 3:
            return None

    return calc_keyfigures_points_2(kf)


def handle_report_reaction(stock, ref_index, cur_date, logger):
    """
    Lade alle Daten um die Reaktionen auf die Quartalszahlen zu berechen.
    Und berechne diese.
    """
    appointments = Appointment.objects \
                              .filter(stock=stock, date__lte=cur_date) \
                              .order_by("-date")
    if len(appointments) == 0:
        dqueue = DataQueue("")
        h = handler_appointments_page(stock.isin, dqueue, logger)
        html = yield from get(request_urls["appointments"] % make_stock_url_ready(stock.name), logger)
        h(html)
        appointments = appointments.all()
        if len(appointments) == 0:
            return None

    new_appointment = appointments[0]

    dtmp = new_appointment.date
    pre_d, post_d = find_pre_post_days(dtmp)
    sp = stock.stockprice_set.filter(date__range=(pre_d, post_d))
    ip = ref_index.indexprice_set.filter(date__range=(pre_d, post_d))

    if len(sp) == 0:
        # Keine Kursdaten vorhanden
        # download!
        dqueue = DataQueue("")
        yield from try_get_stockprices_pre_post_days(stock, pre_d, post_d, dqueue, logger)
        sp = sp.all()
        if len(sp) == 0:
            return None

    if len(ip) == 0:
        # Keine Kursdaten vorhanden
        # download!
        dqueue = DataQueue("")
        yield from try_get_indexprices(ref_index, post_d, dqueue, logger)
        if pre_d.month != post_d.month:
            try_get_indexprices(ref_index, pre_d, dqueue, logger)

        ip = ip.all()
        if len(ip) == 0:
            return None

    if len(sp) != 3 or len(ip) != 3:
        # something went wrong
        return None

    return calc_reaction_points(sp[0].end, sp[2].end,\
                                ip[0].end, ip[2].end)


def calc_stockprice_reversal_points(stockprices, indexprices):
    rs = []
    for i in range(3):
        rs.append(rel_ratio(stockprices[i], stockprices[i+1]))

    ri = []
    for i in range(3):
        ri.append(rel_ratio(indexprices[i], indexprices[i+1]))

    p = 0
    for i,sp in enumerate(rs):
        if sp > ri[i]:
            p = p + 1
        elif sp < ri[i]:
            p = p - 1

    if p == 3:
        return -1
    elif p == -3:
        return 1
    else:
        return 0

def calc_reaction_points(sp_before, sp_after, ip_before, ip_after):
    r1 = rel_ratio(sp_before, sp_after)
    r2 = rel_ratio(ip_before, ip_after)
    tmp = r1 - r2
    return isabove(tmp, -1, 1)

def calc_price_points(p1, p2):
    r = rel_ratio(p1, p2)
    return isabove(r, -5, 5)

def rel_ratio(x, y):
    x = float(x)
    y = float(y)
    return round(((y/x)-1)*100, 5)

def get_ratio(x, y):
    x = float(x)
    y = float(y)
    return round((100/x)*y, 5)

def calc_keyfigures_points(kf):
    kf = tuple(reversed(sorted(kf, key=lambda x: x.year)))
    new_kf = kf[0] 
    r = []
    r.append(isabove(new_kf.roi, 10, 20))
    r.append(isabove(new_kf.return_on_sales, 6, 12))
    r.append(isabove(new_kf.equity_ratio, 15, 25))
    r.append(isabove(new_kf.liquidity, 20, 40))
    r.append(isbelow(new_kf.per, 12, 16))
    r.append(isbelow(new_kf.pcr, 12, 16))

    per_5 = 0
    for f in kf:
        per_5 = per_5 + f.per

    r.append(isbelow(int(per_5/len(kf)), 12, 16))

    pcr_5 = 0
    for f in kf:
        pcr_5 = pcr_5 + f.pcr
    
    r.append(isbelow(int(pcr_5/len(kf)), 12, 16))

    return tuple(r)

def calc_keyfigures_points_2(kf):
    kf = tuple(reversed(sorted(kf, key=lambda x: x.year)))
    new_kf = kf[0] 
    r = []
    r.append(isabove(new_kf.roe, 10, 20))
    r.append(isabove(new_kf.ebit_marge, 6, 12))
    r.append(isabove(new_kf.equity_ratio, 15, 25))
    r.append(isbelow(new_kf.per, 12, 16))

    per_5 = 0
    missing = 0
    for f in kf:
        if f.per is not None:
            per_5 = per_5 + f.per
        else:
            missing = missing + 1

    r.append(isbelow(int(per_5/len(kf)-missing), 12, 16))

    return tuple(r)

def isabove(v, s, e):
    if v is None:
        return None
    if v > e:
        return 1
    elif v >= s and v <= e:
        return 0
    elif v < s:
        return -1


def isbelow(v, s, e):
    if v is None:
        return None
    if v < s:
        return 1
    elif v >= s and v <= e:
        return 0
    elif v > e:
        return -1

class AnalysisResult():
    def __init__(self, stock, date, roi, \
                return_on_sales, equity_ratio, \
                liquidity1, \
                per, per_5y, pcr, pcr_5y, \
                reaction_quartalfigures, \
                stockprice_1y, stockprice_6m, \
                pricemomentum, \
                stockprice_reversal \
                ):
        self.stock = stock
        self.date = date
        self.roi = roi
        self.return_on_sales = return_on_sales
        self.equity_ratio = equity_ratio
        self.liquidity1 = liquidity1
        self.per = per 
        self.per_5y = per_5y
        self.pcr = pcr
        self.pcr_5y = pcr_5y
        self.reaction_quartalfigures = reaction_quartalfigures
        self.stockprice_1y = stockprice_1y
        self.stockprice_6m = stockprice_6m
        self.pricemomentum = pricemomentum
        self.stockprice_reversal = stockprice_reversal
        self.fields = (self.roi, \
               self.return_on_sales, \
               self.equity_ratio, \
               self.liquidity1, \
               self.per, \
               self.per_5y, \
               self.pcr, \
               self.pcr_5y, \
               self.reaction_quartalfigures, \
               self.stockprice_1y, \
               self.stockprice_6m, \
               self.pricemomentum, \
               self.stockprice_reversal)

    def points(self):
        p = 0
        for f in self.fields:
            if f is None:
                continue

            p = p + f

        return p

    def __str__(self):
        r = "(%s, %s" % (self.stock, str(self.fields[0]))
        for f in self.fields[1:]:
            r = r + ", %s" % str(f)

        return "%s)" % r

class AnalysisReturn2():
    def __init__(self, stock , analysis_result, meta):
        self.stock = stock
        self.analysis_result = analysis_result
        self.meta = meta

class AnalysisMeta():
    def __init__(self, company_size):
        self.company_size = company_size

class AnalysisResult2():
    def __init__(self, stock, date, roe, \
                ebit_marge, equity_ratio, \
                per, per_5y, \
                reaction_quartalfigures, \
                stockprice_1y, stockprice_6m, \
                pricemomentum, \
                stockprice_reversal \
                ):
        self.stock = stock
        self.date = date
        self.roe = roe
        self.ebit_marge = ebit_marge
        self.equity_ratio = equity_ratio
        self.per = per 
        self.per_5y = per_5y
        self.reaction_quartalfigures = reaction_quartalfigures
        self.stockprice_1y = stockprice_1y
        self.stockprice_6m = stockprice_6m
        self.pricemomentum = pricemomentum
        self.stockprice_reversal = stockprice_reversal
        self.fields = (self.roe, \
               self.ebit_marge, \
               self.equity_ratio, \
               self.per, \
               self.per_5y, \
               self.reaction_quartalfigures, \
               self.stockprice_1y, \
               self.stockprice_6m, \
               self.pricemomentum, \
               self.stockprice_reversal)

    def points(self):
        p = 0
        for f in self.fields:
            if f is None:
                continue

            p = p + f

        return p

    def __str__(self):
        r = "(%s, %s" % (self.stock, str(self.fields[0]))
        for f in self.fields[1:]:
            r = r + ", %s" % str(f)

        return "%s)" % r
