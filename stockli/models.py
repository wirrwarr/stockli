from django.db import models, connection

countries = (("de", "Deutschland"),)

class Index(models.Model):
    name = models.CharField(max_length=500, verbose_name="name")
    country = models.CharField(max_length=500, verbose_name="country", blank=True, null=True, choices=countries)

    def __str__(self):
        return self.name


class Stock(models.Model):
    isin = models.CharField(max_length=500, verbose_name="isin", blank=False, primary_key=True)
    name = models.CharField(max_length=500, verbose_name="name")
    indizes = models.ManyToManyField(Index, blank=True)

    def __str__(self):
        return "%s %s" % (self.isin, self.name)

class StockQuantity(models.Model):
    stock = models.ForeignKey(Stock)
    date = models.DateField()
    quantity = models.BigIntegerField(verbose_name="Aktienanzahl")

    def __str__(self):
        return "%s %s %s" % (self.stock, self.date, self.quantity)

class ProfitAndLost(models.Model):
    class Meta:
        unique_together = (("year", "stock"),)

    stock = models.ForeignKey(Stock)
    year = models.IntegerField(verbose_name="year")
    ebit = models.FloatField(verbose_name="operativer Gewinn", blank=True, null=True)
    revenue = models.FloatField(verbose_name="Umsatz", blank=True, null=True)

class KeyFigures(models.Model):
    class Meta:
        unique_together = (("year", "stock"),)

    stock = models.ForeignKey(Stock)
    year = models.IntegerField(verbose_name="year")
    per = models.FloatField(verbose_name="kgv", blank=True, null=True)
    pcr = models.FloatField(verbose_name="kcv", blank=True, null=True)
    psr = models.FloatField(verbose_name="kuv", blank=True, null=True)
    pbr = models.FloatField(verbose_name="kbv", blank=True, null=True)
    roi = models.FloatField(verbose_name="roi", blank=True, null=True)
    cashflow = models.FloatField(verbose_name="cashflow", blank=True, null=True)
    liquidity = models.FloatField(verbose_name="Liquidität G1", blank=True, null=True)
    equity_ratio = models.FloatField(verbose_name="Eigenkaptialquote", blank=True, null=True)
    debt_ratio = models.FloatField(verbose_name="Fremdkapitalquote", blank=True, null=True)
    return_on_sales = models.FloatField(verbose_name="Umsatzrentabilität/EBIT-Marge", blank=True, null=True)

    def __str__(self):
        return "%s %s" % (self.stock, self.year)

class KeyFigures2(models.Model):
    class Meta:
        unique_together = (("year", "stock"),)

    stock = models.ForeignKey(Stock)
    year = models.IntegerField(verbose_name="year")
    per = models.FloatField(verbose_name="kgv", blank=True, null=True)
    roe = models.FloatField(verbose_name="roe", blank=True, null=True)
    equity_ratio = models.FloatField(verbose_name="Eigenkaptialquote", blank=True, null=True)
    ebit_marge = models.FloatField(verbose_name="EBIT-Marge", blank=True, null=True)

    def __str__(self):
        return "%s %s" % (self.stock, self.year)
     
class Balance(models.Model):
    class Meta:
        unique_together = (("year", "stock"),)

    stock = models.ForeignKey(Stock)
    year = models.IntegerField(verbose_name="year")
    circulating_assets = models.FloatField(verbose_name="Umlaufvermögen", blank=True, null=True)
    capital_assets = models.FloatField(verbose_name="Anlagevermögen", blank=True, null=True)
    current_liabilities = models.FloatField(verbose_name="kurzfristiges Verbindlichkeiten", blank=True, null=True)
    fixed_liabilities = models.FloatField(verbose_name="langfristige Verbindlichkeiten", blank=True, null=True)
    equity = models.FloatField(verbose_name="Eigenkapital", blank=True, null=True)

class Appointment(models.Model):
    class Meta:
        unique_together = (("date", "stock", "typ"),)

    stock = models.ForeignKey(Stock)
    date = models.DateField(verbose_name="date")
    #1 Jahresabschluss 2 Quartalszahlen
    typ = models.IntegerField(verbose_name="typ")

    def __str__(self):
        return "%s %s %s" % (self.stock, self.date, self.typ)

class Dividend(models.Model):
    year = models.IntegerField(verbose_name="year")
    money = models.FloatField(verbose_name="money")
    stock = models.ForeignKey(Stock)

    def __str__(self):
        return "%s %s %s" % (self.stock, self.year, self.money)

    class Meta:
        unique_together = (("year", "stock"),)

class Market(models.Model):
    id = models.IntegerField(verbose_name="Börsenid", primary_key=True)
    name = models.CharField(max_length=500, verbose_name="Börsenname")

class StockPrice(models.Model):
    class Meta:
        unique_together = (("date", "stock", "market"),)

    stock = models.ForeignKey(Stock)
    market = models.ForeignKey(Market)
    date = models.DateField(verbose_name="date")
    start = models.FloatField(verbose_name="Startwert", blank=True, null=True)
    end = models.FloatField(verbose_name="Schlusswert", blank=True, null=True)
    high = models.FloatField(verbose_name="Höchstwert", blank=True, null=True)
    low = models.FloatField(verbose_name="Niedrigsterwert", blank=True, null=True)
    pieces = models.IntegerField(verbose_name="Stück", blank=True, null=True)

    def __str__(self):
        return "%s %s %s" % (self.stock, self.market, self.date)

class IndexPrice(models.Model):
    class Meta:
        unique_together = (("date", "index"),)

    index = models.ForeignKey(Index)
    date = models.DateField(verbose_name="date")
    start = models.FloatField(verbose_name="Startwert", blank=True, null=True)
    end = models.FloatField(verbose_name="Schlusswert", blank=True, null=True)
    high = models.FloatField(verbose_name="Höchstwert", blank=True, null=True)
    low = models.FloatField(verbose_name="Niedrigsterwert", blank=True, null=True)

    def __str__(self):
        return "%s %s" % (self.index, self.date)

class Portfolio(models.Model):
    name = models.CharField(max_length=500, verbose_name="name")

class PortfolioContent(models.Model):
    class Meta:
        unique_together = (("date", "stock", "portfolio"),)

    date = models.DateTimeField()
    stock = models.ForeignKey(Stock)
    portfolio = models.ForeignKey(Portfolio)

