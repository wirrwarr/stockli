# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "stockli",
    description = "Stock analyise tools",
    version = "0.2",
    packages = find_packages(),
    include_package_data = True,
    author = "Tim Tochtermann",
    author_email = "tim.tochtermann@posteo.de",
    license = "BSD",
    install_requires=[
        "Django>=1.4",
        "aiohttp",
        "beautifulsoup4",
        "psycopg2",
    ],
    #scripts = ["radio_stats/bin/radio_stats"]
)
